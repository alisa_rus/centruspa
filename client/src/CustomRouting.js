import React  from "react";
import { BrowserRouter as Router,useRoutes,} from "react-router-dom";
import Categorie from "./Components/Categorii/Categorie";
import Serviciu  from "./Components/Servicii/Serviciu";
import Acasa from "./Components/Acasa/Acasa";
import Echipa from "./Components/Echipa/Echipa";
import DespreNoi from "./Components/DespreNoi/DespreNoi";
import AdminCategorii from "./Components/Admin/AdminCategorii/AdminCategorii";
import AdminServicii from "./Components/Admin/AdminServicii/AdminServicii";
import AdminAngajati from "./Components/Admin/AdminAngajati/AdminAngajati";
import AdminClienti from "./Components/Admin/AdminClienti/AdminClienti";
import AdminProgramari from "./Components/Admin/AdminProgramari/AdminProgramari";
import AdminRecenzii from "./Components/Admin/AdminRecenzii/AdminRecenzii";
import AdminCategorieSpecificata from "./Components/Admin/AdminCategorii/AdminCategorieSpecificata";
import AdminServiciuSpecificat from "./Components/Admin/AdminServicii/AdminServiciuSpecificat";
import AdminClientSpecificat from "./Components/Admin/AdminClienti/AdminClientSpecificat";
import AdminAngajatSpecificat from "./Components/Admin/AdminAngajati/AdminAngajatSpecificat";
import PanouAdaugareCategorie from "./Components/Admin/AdminCategorii/PanouAdaugareCategorie";
import PanouAdaugareServiciu from "./Components/Admin/AdminServicii/PanouAdaugareServiciu";
import PanouAdaugareClient from "./Components/Admin/AdminClienti/PanouAdaugareClient";
import PanouAdaugareAngajat from "./Components/Admin/AdminAngajati/PanouAdaugareAngajat";
import PanouDedicatAngajat from "./Components/PanouDedicatAngajat/PanouDedicatAngajat";
import PanouAutentificare from "./Components/PanouAutentificare/PanouAutentificare";
import PanouDedicatClient from "./Components/PanouDedicatClient/PanouDedicatClient";



const Routes = () => {

  let routes = useRoutes([
    { path: "/", element: <Acasa/>},
    { path: "/despreNoi", element: <DespreNoi/>},
    { path: "/categorii", element: <Categorie /> },
    { path: "/servicii/:id", element: <Serviciu/> },
    { path:"/echipa",element:<Echipa/>},
    {path:"/autentificare",element:<PanouAutentificare/>},
    {path:"/admin/",element:<AdminCategorii/>},
    {path:"/admin/categorii",element:<AdminCategorii/>},
    {path:"/admin/servicii",element:<AdminServicii/>},
    {path:"/admin/angajati",element:<AdminAngajati/>},
    {path:"/admin/clienti",element:<AdminClienti/>},
    {path:"/admin/clienti/:id",element:<AdminClientSpecificat/>},
    {path:"/admin/clienti/adaugareClient",element:<PanouAdaugareClient/>},
    {path:"/admin/programari",element:<AdminProgramari/>},
    {path:"/admin/recenzii",element:<AdminRecenzii/>},
    { path: "/admin/categorii/:id", element: <AdminCategorieSpecificata/> },
    { path: "/admin/categorii/adaugareCategorie", element: <PanouAdaugareCategorie/> },
    { path: "/admin/servicii/:id", element: <AdminServiciuSpecificat/> },
    { path: "/admin/servicii/adaugareServiciu", element: <PanouAdaugareServiciu/> },
    {path:"/admin/angajati/:id",element:<AdminAngajatSpecificat/>},
    {path:"/admin/angajati/adaugareAngajat",element:<PanouAdaugareAngajat/>},
    {path:"/panouDedicatAngajat/:id",element:<PanouDedicatAngajat/>},
    {path:"/panouDedicatClient/:id",element:<PanouDedicatClient/>},
  ]);
  return routes;
  
};

const CustomRouting = () => {
  return (
    <Router>
      <Routes />
    </Router>
  );
};

export default CustomRouting;