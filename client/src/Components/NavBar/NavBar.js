import React from 'react'
import './NavBar.css'
import { NavLink } from 'react-router-dom';
/* style={isActive => ({border: isActive ? "2px solid white" :"0px ",borderRadius: isActive?"7px":"0px", 
                                padding: isActive?"8px":"0px"})}*/
function NavBar(){
    return(
        <div className="navBar_body">
            <div className="navBar_p navBar_img">
            <img src="/logo2.png" alt="error"className="navBar_img" />
            </div>
            <NavLink to="/" className="navBar_p">Acasa</NavLink>
            <NavLink  to="/despreNoi" className="navBar_p">Despre noi</NavLink>
            <NavLink  to="/categorii" className="navBar_p">Serviciile noastre</NavLink>
            <NavLink  to="/echipa" className="navBar_p">Echipa noastra</NavLink>
            <NavLink to="/autentificare" className="navBar_p">Contul meu</NavLink>
        </div>
    )
    
}
export default NavBar;