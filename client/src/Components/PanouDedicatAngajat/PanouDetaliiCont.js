import React, { useState,useEffect } from 'react';
import Axios from 'axios';
import './PanouDetaliiCont.css';

function PanouDetaliiCont(props){

    const [id]=useState(props.Cod_angajat);
    const [nume,setNume]=useState('');
    const [prenume,setPrenume]=useState('');
    const [telefon,setTelefon]=useState('');
    const [email,setEmail]=useState('');
    const [parola,setParola]=useState('');
    const [descriere,setDescriere]=useState('');
    const [file,setFile]=useState();
    const [fileName,setFileName]=useState('');
    const [flagParola,setFlagParola]=useState(false);

    const saveFile = (e) => {
        setFile(e.target.files[0]);
        setFileName(e.target.files[0].name);
    };

    const adaugaPoza=()=>{
         
        const formData = new FormData();
        formData.append("file", file);
        formData.append("Imagine", fileName);
        formData.append("Nume",nume);
        formData.append("Prenume",prenume);
        formData.append("Telefon",telefon);
        formData.append("Email",email);
        formData.append("Parola",parola);
        formData.append("Descriere",descriere)
        formData.append("Flag_parola",flagParola);

        if(nume===''||prenume===''||email===''||parola===''||telefon===''||descriere===''||fileName==='')
        alert("Completati toate campurile.");
        else {
            Axios.put("http://localhost:3001/user/angajat/modificareAngajat",formData).then((response) =>
            {
                if(response.data==="Cont angajat modificat cu succes.")
                window.location.reload(true);
            });
        }
    }

    const modificareAngajat=()=>{

        if(nume===''||prenume===''||email===''||parola===''||telefon===''||descriere===''||fileName==='')
            alert("Completati toate campurile.");
        else {
            Axios.put("http://localhost:3001/user/angajat/modificareAngajat",{
                "file":file,
                "Imagine":fileName,
                "Nume":nume,
                "Prenume":prenume,
                "Telefon":telefon,
                "Email":email,
                "Parola":parola,
                "Descriere":descriere,
                "Cod_utilizator":id,
                "Flag_parola":flagParola,
            }).then((response) =>
            {
                alert(response.data);
                if(response.data==="Cont angajat modificat cu succes."){
                    adaugaPoza();                
                }              
            });
        }       
    }

    useEffect(()=>{

        const getInfo=()=>{
            Axios.get(`http://localhost:3001/user/angajatById/${id}`).then((response) => {          
                if(response.data.length!==0){
                    setNume(response.data[0].Nume);
                    setPrenume(response.data[0].Prenume);
                    setTelefon(response.data[0].Telefon);
                    setEmail(response.data[0].Email);
                    setParola(response.data[0].Parola);
                    setDescriere(response.data[0].Descriere);
                    setFileName(response.data[0].Imagine);
                }
            });    
        }   
        getInfo(); 

    },[id]);
    return(
        <div className="panouDetaliiContMainDiv">
            <div className="panouDetaliiContDivNume">
                <label className="labelNume">Nume: </label>
                <textarea className="textNume" defaultValue={nume} onChange={(e)=>{setNume(e.target.value)}}></textarea>
            </div>
            <div className="panouDetaliiContDivPrenume">
                <label className="labelPrenume">Prenume: </label>
                <textarea className="textPrenume" defaultValue={prenume} onChange={(e)=>{setPrenume(e.target.value)}}></textarea>
            </div>
            <div className="panouDetaliiContDivTelefon">
                <label className="labelTelefon">Telefon: </label>
                <textarea className="textTelefon" defaultValue={telefon} onChange={(e)=>{setTelefon(e.target.value)}}></textarea>
            </div>
            <div className="panouDetaliiContDivEmail">
                <label className="labelEmail">Email: </label>
                <textarea className="textEmail" defaultValue={email} onChange={(e)=>{setEmail(e.target.value)}}></textarea>
            </div>  
            <div className="panouDetaliiContDivParola">
                <label className="labelParola">Parola: </label>
                <input type='password'className="textParola" defaultValue={parola} onChange={(e)=>{setParola(e.target.value);setFlagParola(true)}}></input>
            </div>
            <div className="panouDetaliiContLastDiv">
                <div className="panouDetaliiContImagine">
                    <label className="labelImagine">Imagine: </label>
                    <div className="personal-image">
                        <label className="label">
                            <input type="file" onChange={saveFile}/>
                            <figure className="personal-figure">
                            <img  src={`/images/echipa/${fileName}`} className="personal-avatar" alt="avatar" />
                            <figcaption className="personal-figcaption">
                                <img src="/camera-white.jpg" alt=""/>
                            </figcaption>
                            </figure>
                        </label>
                    </div>
                </div>
                <div className="panouDetaliiContDivDescriere">
                    <label className="labelDescriere">Descriere: </label>
                    <br></br>
                    <textarea className="textDescriere" defaultValue={descriere} onChange={(e)=>{setDescriere(e.target.value)}}></textarea>
                    <button className="butonActualizare" onClick={modificareAngajat}>Actualizeaza informatiile</button>
                </div>
            </div>
            
        </div>
    )
}
export default PanouDetaliiCont;