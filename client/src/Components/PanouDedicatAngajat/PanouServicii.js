import React, { useState,useEffect } from 'react';
import PanouServiciuAngajat from './PanouServiciuAngajat';
import Axios from 'axios';
import "./PanouServicii.css";

function PanouServicii(props){

    const [Cod_angajat]=useState(props.Cod_angajat);
    const [servicii,setServicii]=useState([]);

    useEffect(()=>{
        const getServiciiAngajat=(id)=>{
            Axios.get(`http://localhost:3001/user/echipa/serviciiAngajat/${id}`).then((response) => {
                setServicii(response.data);
            });
        }
        getServiciiAngajat(Cod_angajat);
    },[Cod_angajat])
    return(
        <>
            <p className='pServiciileMele'>Serviciile mele</p>
            <div className="PanouServiciiMainDiv">
                {servicii.map((val)=>{
                    return(
                        <PanouServiciuAngajat key={val.Cod_serviciu} Denumire={val.Denumire} Imagine={val.Imagine}/>
                    )
                })}
            </div>
        </>

    )
}
export default PanouServicii;

