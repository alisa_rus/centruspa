import Axios  from 'axios';
import React,{useState,useEffect} from 'react';
import PanouRecenzieAngajat from './PanouRecenzieAngajat';
import "./PanouRecenzii.css";

function PanouRecenzii(props){
    
    const[id]=useState(props.Cod_angajat);
    const [recenzii,setRecenzii]=useState([]);
    useEffect(()=>{
        Axios.get(`http://localhost:3001/user/echipa/recenziiAngajat/${id}`).then((response)=>{
            setRecenzii(response.data);
        })
    },[id]);

    return(
        <div className="panouRecenziiMainDiv">
            <p className="pRecenziileMele">Recenziile mele</p>
            {recenzii.map((val)=>{
                return(
                    <PanouRecenzieAngajat Nota={val.Nota} Mesaj={val.Mesaj} key={val.Cod_recenzie}/>      
                )
            })}
            
        </div>
    )
}
export default PanouRecenzii;
