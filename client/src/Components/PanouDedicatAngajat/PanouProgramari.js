import  Axios  from 'axios';
import React,{useState,useEffect} from 'react';
import {AiFillCheckCircle} from "react-icons/ai";
import {AiFillCloseCircle} from "react-icons/ai";
import Pdf from "react-to-pdf";
import "./PDF.css";
import "./PanouProgramari.css";
const ref = React.createRef();


function PanouProgramari(props){

    const [id]=useState(props.Cod_angajat);
    const [programari,setProgramari]=useState([]);
    const [visibleFactura,setVisibleFactura]=useState(false);
    const [visibleDetalii,setVisibleDetalii]=useState(false);
    const [factura,setFactura]=useState();
    const [client,setClient]=useState([]);

    useEffect(()=>{
        Axios.get(`http://localhost:3001/user/echipa/programariAngajat/${id}`).then((response)=>{
            setProgramari(response.data);
        })
    },[id]);

    const sendEmail=(id,zi,ora)=>{
        Axios.get(`http://localhost:3001/user/clientById/${id}`).then((response)=>{
            Axios.post("http://localhost:3001/sendEmail/send",{
                "destinatar":response.data[0].Email,
                "textEmail":"Test: Programarea din data de "+zi+" de la ora "+ora.substr(0,5)+" a fost confirmata. Te asteptam cu drag! "
            }).then((response)=>{
                if(response.data==='Email trimis.')
                    window.location.reload(true);
            }); 
        });      
    }
    const confirmaProgramare=(id,client,zi,ora)=>{
        Axios.put(`http://localhost:3001/programari/confirmaProgramare/${id}`).then((response)=>{
            alert(response.data);
            if(response.data==='Programare confirmata cu succes. '){
                sendEmail(client,zi,ora);           
            }
        })
    }
    const finalizeazaProgramare=(id)=>{
        
        Axios.put(`http://localhost:3001/programari/finalizeazaProgramare/${id}`).then((response)=>{
            alert(response.data);
            if(response.data==='Programare finalizata cu succes. '){ 
                setVisibleFactura(!visibleFactura);        
            }
        })
    }
    const sendEmailStergere=(id,zi,ora)=>{
        Axios.get(`http://localhost:3001/user/clientById/${id}`).then((response)=>{
            console.log(response.data[0].Email);
            Axios.post("http://localhost:3001/sendEmail/send",{
                "destinatar":response.data[0].Email,
                "textEmail":"Test: Programarea din data de "+zi+" de la ora "+ora.substr(0,5)+" a fost anulata. Multumim pentru intelegere."
            }).then((response)=>{
                if(response.data==='Email trimis.')
                    window.location.reload(true);
            }); 
        });      
    }
    const stergereProgramare=(id,Cod_client,zi,ora)=>{
        Axios.delete(`http://localhost:3001/programari/stergereProgramare/${id}`).then((response)=>{
            alert(response.data);
            if(response.data==='Programare stearsa cu succes.')
                {
                    sendEmailStergere(Cod_client,zi,ora);
                    
                }
        });  
    }
    const getInfoClienti=(id)=>{
        Axios.get(`http://localhost:3001/user/clientById/${id}`).then((response)=>{
            setClient(response.data);
            console.log(response.data);
        })
    }
    return(
        <>
            <div className='panouProgramariMainDiv'>
                <table className="tabelProgramariAngajat">
                    <thead>
                        <tr className="randTabelProgramariAngajat">
                            <th className="hTabelProgramariAngajat">Serviciu</th>
                            <th className="hTabelProgramariAngajat">Status</th>
                            <th className="hTabelProgramariAngajat">Data</th>
                            <th className="hTabelProgramariAngajat">Ora</th>
                            <th className="hTabelProgramariAngajat">Confirma/anuleaza programare</th>
                            <th className="hTabelProgramariAngajat">Finalizeaza programare</th>
                            <th className="hTabelProgramariAngajat">Vizualizeaza detalii</th>
                        </tr>
                    </thead>
                    <tbody>                                  
                        {programari.map((val)=>{     
                            let x=val.OraStart;      
                        return(                   
                                <tr className="randDateTabelProgramariAngajat" key={val.Cod_programare}>
                                    <td>{val.Denumire}</td>
                                    <td>{val.Status}</td>
                                    <td>{val.Zi}</td>
                                    <td>{x.substr(0,5)}</td>
                                    <td>
                                        <button className="tdButonProgramariButtonAngajat1">
                                            <div className="check">
                                                <AiFillCheckCircle size={20} className="iconCheck" onClick={()=>{confirmaProgramare(val.Cod_programare,val.Cod_client,val.Zi,val.OraStart)}}/>
                                            </div>
                                            <div className="close">
                                                <AiFillCloseCircle size={20} className="iconClose" onClick={()=>{stergereProgramare(val.Cod_programare,val.Cod_client,val.Zi,val.OraStart)}}/>
                                            </div>
                                        </button>
                                    </td>
                                    <td>
                                        <button className="tdButonProgramariButtonAngajat" onClick={()=>{getInfoClienti(val.Cod_client);setFactura(val);finalizeazaProgramare(val.Cod_programare)}}>
                                            FINALIZEAZA
                                        </button>
                                    </td>
                                    <td>
                                        <button className="tdButonProgramariButtonAngajat" onClick={()=>{getInfoClienti(val.Cod_client);setFactura(val);setVisibleDetalii(!visibleDetalii)}}>VIZUALIZEAZA</button>
                                    </td>
                                </tr>       
                        )
                        })}
                    </tbody>
                </table>             
            </div>
            {visibleFactura && (
                <>
                    <div className="Post"  ref={ref} >
                        <p className='closeP' onClick={()=>{setVisibleFactura(!visibleFactura);window.location.reload(true)}}>X</p>
                        <div className='PDFfirstrow'>
                            <div className='logo'>
                                <img src="/logo2.png"  alt="err" />
                            </div>
                            <div className='PDFfirstrowsecondcolumn'>
                                <h1 className='title'>Factura</h1>
                                <h2 className='dataEmiterii'>Data emiterii: {Date().toLocaleString().substring(4,15)} </h2>
                                <div className='totalPlata'>
                                    <p className='totalPlataP'>TOTAL PLATA</p>
                                    <p className='totalPlataSuma'>{factura.Pret} Lei</p>
                                </div>
                            </div>
                        </div>
                        <div className='PDFsecondrow'>
                            <div className='PDFsecondrowcolumn'>
                                <p className='furnizorclient'>Furnizor</p>
                                <hr className='furnizorclienthr'></hr>
                                <div className='furnizorclientdetalii'>
                                    <p className='denumirefurnizorclient'>CENTRU SPA CLUJ-NAPOCA</p>
                                    <p className='CIF'>CIF: <b>0000000000</b></p>
                                    <p className='adresa'>Adresa: Oras Cluj-Napoca, Str Mircea Eliade, Nr 454C, Jud. Cluj-Napoca</p>
                                    <p className='IBAN'>IBAN(RON) <b>R000000000000000000000</b></p>
                                    <p className='Banca'>Banca: BANCA TRANSILVANIA</p>
                                </div>
                            </div>
                            <div className='PDFsecondrowcolumn'>
                                <p className='furnizorclient'>Client</p>
                                <hr className='furnizorclienthr'></hr>
                                {client.map((client)=>{
                                    return(
                                    <div className='furnizorclientdetalii'>
                                        <p className='denumirefurnizorclient'>{client.Nume} {client.Prenume}</p>
                                        <p className='telefon'>Telefon: <b>{client.Telefon}</b></p>
                                        <p className='mail'>Email: <b>{client.Email}</b></p>
                                    </div>
                                    )
                                })}
                                
                            </div>
                        </div>
                        <div className='mainDiv'>
                            <table className='mainDivTable'>
                                <thead className='mainDivHeadTable'>
                                    <hr className='hrTable'></hr>
                                    <tr>
                                        <th>Nr.crt</th>
                                        <th>Denumirea produselor sau a serviciilor</th>
                                        <th>U.M.</th>
                                        <th>Cant.</th>
                                        <th>Pret unitar (Lei)</th>
                                        <th>Valoare (Lei)</th>
                                    </tr>
                                    <hr className='hrTable'></hr>
                                </thead>
                                <tbody className='mainDivBodyTable'>
                                    <tr>
                                        <td>1</td>
                                        <td>{factura.Denumire}</td>
                                        <td>servicii SPA</td>
                                        <td>1</td>
                                        <td>{factura.Pret}</td>
                                        <td>{factura.Pret}</td>
                                    </tr>
                                    <hr className='hrTable2'></hr>
                                </tbody>
                            </table>
                        </div>
                        <div className='tbodyfinal'>
                                <p className='tbodyfinalP'>TOTAL PLATA</p>
                                <p className='tbodyfinalS'>{factura.Pret} Lei</p>
                        </div>
                    </div>
                    <Pdf targetRef={ref} filename="factura">
                        {({ toPdf }) => <button onClick={toPdf} className="buttonPDF">Descarca PDF</button>}
                    </Pdf>
                </>
                )
            }
            {visibleDetalii&&(
                <div className='divDetalii'>
                    <p className='closeDetalii' onClick={()=>setVisibleDetalii(!visibleDetalii)}>X</p>
                    <div className='detalii'>
                        <div className='detaliiRow'>
                            <p className='labelData'>
                                Data:
                            </p>
                            <p className='contentData'>
                                {factura.Zi}
                            </p>
                        </div>
                        <div className='detaliiRow'>
                            <p className='labelOra'>
                                Ora:
                            </p>
                            <p className='contentOra'>
                                {factura.OraStart}
                            </p>
                        </div>
                        <div className='detaliiRow'>
                            <p className='labelPret'>
                                Pret:
                            </p>
                            <p className='contentPret'>
                                {factura.Pret}
                            </p>
                        </div>
                        <div className='detaliiRow'>
                            <p className='labelStatus'>
                                Status:
                            </p>
                            <p className='contentStatus'>
                                {factura.Status}
                            </p>
                        </div>
                        <div className='detaliiRow'>
                            <p className='labelServiciu'>
                                Serviciu:
                            </p>
                            <p className='contentServiciu'>
                                {factura.Denumire}
                            </p>
                        </div>
                        <p className='labelClient'>
                                Detalii client:
                        </p>
                        <hr className='hrDetalii'></hr>
                        {client.map((client)=>{
                            return(
                            <div className='detaliiRow'>
                                <p className='contentNume'>
                                    {client.Nume} {client.Prenume} | 
                                </p>
                                <p className='contentNume'>
                                    | {client.Email} |
                                </p>
                                <p className='contentNume'>
                                    | {client.Telefon}
                                </p>
                            </div>
                            )
                        })}
                        
                    </div>
                </div>
            )

            }                     
        </>
    )
}
export default PanouProgramari;