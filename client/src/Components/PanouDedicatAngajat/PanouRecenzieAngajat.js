import React,{useState} from 'react'

function PanouRecenzieAngajat(props){
    
    let x=props.Mesaj;
    const [visible,setVisible]=useState(false);
    return(
        <div className="panouRecenzieMainDiv">
            <div className="panouRecenzieMainDivNota">
                <label className="panouRecenzieMainDivLabel">Nota primita: </label>
                <div className="panouRecenzieMainDivNotaText">{props.Nota}</div>
            </div>
            <br></br>
            <div className="panouRecenzieMainDivMesaj">
                <label className="panouRecenzieMainDivLabel">Mesaj: </label>
                <div className="panouRecenzieMainDivMesajText" onClick={()=>{setVisible(true)}}> {x.substr(0,87)}...</div>
            </div>
            <hr className="hrPanouRecenzii"></hr>
            {visible && (
                <div className="popUpPanouRecenzieAngajat">
                    <br></br>
                    <p className="popUpPanouRecenzieAngajatClose" onClick={()=>{setVisible(false)}}>X</p>
                    <textarea className="popUpPanouRecenzieAngajatText" defaultValue={x}></textarea>
                </div>
            )}
        </div>
    )

}
export default PanouRecenzieAngajat;



