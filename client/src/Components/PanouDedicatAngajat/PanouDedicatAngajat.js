import React, { useEffect,useState } from 'react';
import { useParams} from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import PanouDetaliiCont from './PanouDetaliiCont';
import PanouServicii from './PanouServicii';
import PanouProgramari from './PanouProgramari';
import PanouRecenzii from './PanouRecenzii';
import "./PanouDedicatAngajat.css";
import PanouAutentificare from '../PanouAutentificare/PanouAutentificare';

function PanouDedicatAngajat(){


    let {id}=useParams(); //id-ul angajatului il iau din ruta, atunci cand se face autentificarea
    let navigate=useNavigate();

    const logout = async (e) => {
        navigate('/');
        window.localStorage.clear();
       
    };

    const [isLogged,setLogged]=useState(false);
  
    const checkIfLogin=()=>{
        if(window.localStorage.getItem("token") && window.localStorage.getItem("role")==='2' && window.localStorage.getItem("id")===id)
            setLogged(true);
        else 
            {
                setLogged(false);
                navigate('/autentificare')
            }
    }

    useEffect(()=>{
        checkIfLogin();
    })

    return(
        <>
        {isLogged && (
            <div className="panouDedicatAngajatMainDiv">
            <div className="panouDedicatAngajatMainDivFirstRow">
                <div className="panouDedicatAngajatMainDivFirstRowFirstColumn">
                    <PanouDetaliiCont Cod_angajat={id}/>
                </div>
                <div className="panouDedicatAngajatMainDivFirstRowSecondColumn">
                    <PanouServicii Cod_angajat={id}/>
                </div>
            </div>
            <div className="panouDedicatAngajatMainDivSecondRow">
                <div className="panouDedicatAngajatMainDivSecondRowFirstColumn">
                    <PanouProgramari Cod_angajat={id}/>
                </div>
                <div className="panouDedicatAngajatMainDivSecondRowSecondColumn">   
                    <PanouRecenzii Cod_angajat={id}/>
                </div>
            </div>
            <div className="panouDedicatAngajatMainDivThirdRow">
                <button className="panouDedicatAngajatButonDeconectare" onClick={()=>{logout()}}>DECONECTARE</button>
            </div>
            
        </div>
        )}
        {
            !(isLogged) && (
            <div className='panouAutentificareNotLogged'>
                <PanouAutentificare/>
            </div>)
        }
        </> 
    )
}
export default PanouDedicatAngajat;
