import React from 'react'
import "./PanouServiciuAngajat.css";

function PanouServiciuAngajat(props){
    return(
        <div className="panouServiciuAngajatMainDiv">
            <div className="panouServiciuAngajatImg">
                <img  src={`/images/servicii/${props.Imagine}`} alt="error" className='panouServiciuAngajatImagine'/>
            </div>
            <div className="panouServiciuAngajatDenumire">
                {props.Denumire}
            </div>  
        </div>
    )
}
export default PanouServiciuAngajat;