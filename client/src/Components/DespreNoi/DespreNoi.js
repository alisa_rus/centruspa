import React from "react";
import { useNavigate } from 'react-router-dom';
import NavBar from "../NavBar/NavBar";
import Footer from '../Footer/Footer';
import "./DespreNoi.css";

const texte=[
    "Incercam sa oferim o experienta minunata clientilor nostri. Specialistii complexului nostru sunt atenti asupra tuturor detaliilor si se dedica in totalitate nevoilor clientilor. Clientul este unic si are nevoie de o experienta personalizata. Corpul tau are nevoie de relaxare. Alege sa petreci momente de exceptie in complexul nostru. Serviciile noastre au performante vizibile inca de la primele sedinte. O sedinta saptamanala la complex te va ajuta sa reduci oboseala, stresul si problemele zilnice! Profita repede!!",
    "Vei fi mai energic si mai sanatos daca apelezi la serviciile noastre. Beneficiaza de un tratament la spa si vei vedea efecte benefice imediate. Incercam sa va oferim o experienta incantatoare prin prezenta aromelor, a muzicii, a culorilor si a mediului confortant. Dorim sa fim inspiratia ta in conceperea unui stil de viata corespunza-tor. Scapa de stres si problemele cotidiene si ofera atentie organismului tau. Meriti experiente personalizate !!!",
    "Folosim tehnologie de ultima generatie. Specialistii nostri sunt acreditati si participa constant la cursuri pentru imbunatatirea serviciilor practicate. Complexul nostru este verificat saptamanal din punctul de vedere al igienei si al utilizarii aparatelor noastre. Dezinfectam fiecare spatiu dupa folosire, astfel incat clientii nostri sa fie in siguranta. Serviciile sunt diverse: masaje, tratamente corporale, imbaieri, proceduri sport, sauna. Alege ritualul favorit si lasa-te pe mana noastra, caci oferim seriozitate!",
    "Complexul nostru s-a nascut din dorinta de a oferi o stare de bine clientilor, de a ii ajuta sa depaseasca problemele cotidiene si sa se concentreze pe starile lor de bine. Suntem un complex acreditat, iar echipa noastra de specialisti ofera clientilor clipe de relaxare deplina. Folosim ingredientele naturale, iar aparatele noastre de ultima generatie vin in sprijinul persoanelor cu diferite nevoi. Profita de experiente faine si programeaza-te din timp. Ai grija de tine!"
];

function DespreNoi(){

    let navigate=useNavigate();

    const redirect = () => {
    
        navigate(`/categorii`);
    }

    const redirectToEchipa = () => {
    
        navigate(`/echipa`);
    }

    return(
        <>
        <NavBar/>    
        <div className="rowDespreNoi1">
            <div className="columnDespreNoi1">
                <p className="columnDespreNoiText">{texte[3]}</p>
            </div>
            <div className="columnDespreNoi1">
                <img className="imgDespreNoi" src="images/despreNoi/despreNoi1.jpg" alt="error" onClick={redirect}/>
            </div>
        </div>
        <div className="rowDespreNoi2">
            <div className="columnDespreNoi2">
                <img className="imgDespreNoi" src="images/despreNoi/despreNoi2.jpg" alt="error" onClick={redirect}/>
            </div>
            <div className="columnDespreNoi2">
                <p className="columnDespreNoiText">{texte[2]}</p>
            </div>
        </div>
        <h1 className="despreNoiH1"  onClick={redirectToEchipa}>Fa cunostinta cu echipa noastra!</h1>
        <div className="rowDespreNoi1">
            <div className="columnDespreNoi1">
                <p className="columnDespreNoiText">{texte[1]}</p>
            </div>
            <div className="columnDespreNoi1">
                <img className="imgDespreNoi" src="images/despreNoi/despreNoi3.jpg" alt="error" onClick={redirect}/>
            </div>
        </div>
        <div className="rowDespreNoi2">
            <div className="columnDespreNoi2">
                <img className="imgDespreNoi" src="images/despreNoi/despreNoi4.jpg" alt="error" onClick={redirect}/>
            </div>
            <div className="columnDespreNoi2">
                <p className="columnDespreNoiText">{texte[0]}</p>
            </div>
        </div>
        <Footer/>
        </>
    )

}
export default DespreNoi;