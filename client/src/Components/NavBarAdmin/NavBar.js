import React from 'react'
import './NavBar.css'
import { NavLink } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
function NavBar(){

    let navigate=useNavigate();

    const logout = async (e) => {
        navigate('/');
        window.localStorage.clear();
       
    };

    return(
        <div className="navBar_bodyAdmin">
            <div className="navBarRow">
                <div className='logo'>
                    <img src="/logo2.png" alt="error"className="navBar_imgadmin" />
                </div>
                <div className="navBarColumn">
                    <NavLink to="/admin/categorii" className="navBarNavLink">Categorii</NavLink>
                </div>
                <div className="navBarColumn">
                    <NavLink  to="/admin/servicii" className="navBarNavLink">Servicii</NavLink>
                </div>
                <div className="navBarColumn">
                    <NavLink  to="/admin/angajati" className="navBarNavLink">Angajati</NavLink>
                </div>
                <div className="navBarColumn">
                    <NavLink  to="/admin/clienti/" className="navBarNavLink">Clienti</NavLink>
                </div>
                <div className="navBarColumn">
                    <NavLink  to="/admin/programari" className="navBarNavLink">Programari</NavLink>
                </div>
                <div className="navBarColumn">
                    <NavLink  to="/admin/recenzii" className="navBarNavLink">Recenzii</NavLink>
                </div>
                <div className="navBarColumn">
                    <button className="navBarNavButton" onClick={()=>{logout()}}>Deconectare</button>
                </div>                             
            </div>
        </div>
    )
    
}
export default NavBar;