import React from 'react'
import './CardServiciu.css'


function CardServiciu(props){
    
    return(
     <div className="card">
            <div className="card_body">
                <p className="card_title">{props.Denumire} </p >
                <div className="card_description">
                    {props.Descriere}
                </div>
                <div className="card_benefits">
                    <p className="card_benefits_p">Beneficii</p>
                    {props.Beneficii}
                </div>
                <img className="card_image" src={`/images/servicii/${props.Imagine}`} alt="error"/>
                <div className="card_price_duration">
                <div className="card_price">{props.Pret} ron</div>
                <div className="card_duration">{props.Durata} minute</div>
                </div>            
            </div>
          
    </div>
    )
}
export default CardServiciu;