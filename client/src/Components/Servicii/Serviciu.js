import React, { useState, useEffect} from 'react';
import Axios from 'axios';
import CardServiciu from './CardServiciu';
import NavBar from '../NavBar/NavBar';
import Footer from '../Footer/Footer';
import './CardServiciu.css'
import '../NavBar/NavBar.css'
import { useParams } from 'react-router-dom';

function Serviciu(){
    
    let { id } = useParams();
    const [servicii,setServicii]=useState([]);
 
    const getServiciiByCategory=(id)=>{
         Axios.get(`http://localhost:3001/servicii/dupaCategorie/${id}`).then((response) => {
         setServicii(response.data);
         });
    }

    useEffect(()=>{    
        getServiciiByCategory(id);
    },[id]);
   
    return(
        <div className="serviciiBody">
            <NavBar/>
            <div className='serv'>
            {servicii.map((val)=>{
                return(
                            <CardServiciu key={val.Cod_serviciu}  Cod_serviciu={val.Cod_serviciu}
                            Descriere={val.Descriere} Denumire={val.Denumire} Imagine={val.Imagine} 
                            Beneficii={val.Beneficii} Durata={val.Durata} Pret={val.Pret}/>
                        )
            })}
            </div>
            <Footer/>   
        </div>)

}

export default Serviciu