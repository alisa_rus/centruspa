import React, { useState } from 'react';
import {GoLocation} from 'react-icons/go';
import {BiTimeFive} from 'react-icons/bi';
import {AiOutlinePhone} from 'react-icons/ai';
import {AiOutlineMail} from 'react-icons/ai';
import Axios from 'axios';
import './Footer.css';
function Footer(){

    const [nume,setNume]=useState('');
    const [telefon,setTelefon]=useState('');
    const [email,setEmail]=useState('');
    const [mesaj,setMesaj]=useState('');

    const sendEmail=()=>{
            Axios.post("http://localhost:3001/sendEmail/send",{
                "destinatar":"centruspatest@gmail.com",
                "textEmail":`Utilizatorul ${nume}, avand emailul ${email} si numarul de telefon ${telefon} a scris urmatorul mesaj: "${mesaj}"`
            }).then((response)=>{
                console.log(response.data);
                if(response.data==='Email trimis.')
                    window.location.reload(true);
            });       
    }
    return(
        <div className='footerMainDiv'>
            <div className='footerInfo'>
                <p className='contactUs'>Contactati-ne!</p>
                <p className='contactMesaj'> Ne puteti contacta prin email, telefon sau fizic la sediul centrului nostru spa.</p>
                <p className='location'><GoLocation style={{marginBottom:"-3px",marginRight:"10px"}}/>Cluj-Napoca, Strada Mircea Eliade, Nr 454C, Judetul Cluj-Napoca, Romania</p>
                <p className='program'><BiTimeFive style={{marginBottom:"-3px",marginRight:"10px"}}/>Program: Luni-Vineri, intre orele 10:00-18:00</p>
                <p className='phone' href=""><AiOutlinePhone style={{marginBottom:"-3px",marginRight:"10px"}}/>Telefon: 0700000000</p>
                <p></p>
                <a className='email' href="mailto:centruspatest@gmail.com"><AiOutlineMail style={{marginBottom:"-3px",marginRight:"10px"}}/>Email: centruspatest@gmail.com </a>
            </div>
            <div className='separate'>

            </div>
            <div className='footerContact'>
                <p className='leaveMessage'>Lasa-ne un mesaj aici, iar noi vom raspunde cat mai repede posibil.</p>
                <div className='formularContact'>
                    <input type='text' placeholder='Nume' className='numeContact' onChange={(e)=>{setNume(e.target.value)}}></input>
                    <input type='text' placeholder='Telefon' className='telefonContact' onChange={(e)=>{setTelefon(e.target.value)}}></input>
                    <input type='text' placeholder='Email' className='emailContact' onChange={(e)=>{setEmail(e.target.value)}}></input>
                    <input type='text' placeholder='Mesaj' className='mesajContact' onChange={(e)=>{setMesaj(e.target.value)}}></input>
                    <button className='sendMessage' onClick={sendEmail}>Trimite mesaj</button>
                </div>
            </div>
        </div>
    )
}
export default Footer;