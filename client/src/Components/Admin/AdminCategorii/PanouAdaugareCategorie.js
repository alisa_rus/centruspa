import  Axios  from 'axios';
import React, {useState,useEffect} from 'react';
import NavBar from '../../NavBarAdmin/NavBar';
import {useNavigate} from 'react-router-dom';
import PanouAutentificare from '../../PanouAutentificare/PanouAutentificare';

function PanouAdaugareCategorie(){

    const [denumireNoua, setDenumireNoua]=useState('');
    const [descriereNoua,setDescriereNoua]=useState('');
    const [file, setFile] = useState();
    const [fileName, setFileName] = useState("");
    const [isLogged,setLogged]=useState(false);
  
    const checkIfLogin=()=>{
        if(window.localStorage.getItem("token") && window.localStorage.getItem("role")==='1')
            setLogged(true);
        else 
        {
            setLogged(false);
            navigate('/autentificare');
        }
    }  
    
    const saveFile = (e) => {
        setFile(e.target.files[0]);
        setFileName(e.target.files[0].name);
    };
 
    let navigate= useNavigate();

    const redirect = () => {
        navigate(`/admin/categorii`);
    }

    const adaugareCategorie= async(e)=>{

        const formData = new FormData();
        formData.append("file", file);
        formData.append("fileName", fileName);
        formData.append("Denumire",denumireNoua);
        formData.append("Descriere",descriereNoua);
        
        if(denumireNoua==='' ||descriereNoua===''||fileName==='')
            alert("Completati toate campurile.");
        else {
        Axios.post("http://localhost:3001/categorii/adaugareCategorie",formData).then((response) => {
                alert(response.data);
                if(response.data==="Categorie inserata cu succes.")
                    redirect();
            });
        }
    }
    useEffect(()=>{    
        checkIfLogin();
    });
    return(
        <>
        {
            isLogged && (
                <div className="adminCategoriiPanou">
                <div className="adminCategoriiNavBar">
                    <NavBar/>
                </div>
                <div className="adminAdaugareClientColumn">
                <p className="panouClientP"> Completeaza detaliile noii categorii</p>      
                    <div className="firstRowPanouCategorii">
                        <div className="firstRowPanouCategoriiFirstColumn">
                            <label className="firstRowPanouCategoriiFirstColumnLabel">Denumire: </label>
                        </div>
                        <div className="firstRowPanouCategoriiSecondColumn">
                            <textarea className="firstRowPanouCategoriiTextBoxDenumire" name="denumire" rows="1" 
                                    onChange={(event)=>{setDenumireNoua(event.target.value);}}>
                            </textarea>
                        </div>
                    </div>
                    <div className="firstRowPanouCategorii">
                        <div className="firstRowPanouCategoriiFirstColumn">
                            <label className="firstRowPanouCategoriiFirstColumnLabel">Descriere: </label>
                        </div>
                        <div className="firstRowPanouCategoriiSecondColumn">
                            <textarea className="firstRowPanouCategoriiTextBoxDescriere" name="descriere" rows="1" 
                                    onChange={(event)=>{setDescriereNoua(event.target.value);}}>
                            </textarea>
                        </div>
                    </div>
                    <div className="firstRowPanouCategorii">
                        <div className="firstRowPanouCategoriiFirstColumn">
                            <label className="firstRowPanouCategoriiFirstColumnLabel">Imagine: </label>
                        </div>
                        <div className="firstRowPanouCategoriiSecondColumn">
                        <input type="file" onChange={saveFile} />
                        </div>
                    </div>
                    <div className="secondRowPanouCategorieAdd">
                        <button className="firstRowPanouClientInsertButton" onClick={adaugareCategorie}>Adauga categorie noua</button>
                    </div>
                </div>
            </div>
            )
        }
        {
            !isLogged && (
                <PanouAutentificare/>
            )
        }
            
        </>
    )

}
export default PanouAdaugareCategorie;