import React,{useState,useEffect} from 'react'
import Axios from 'axios';
import { useNavigate } from 'react-router-dom';
import NavBar  from '../../NavBarAdmin/NavBar';
import './AdminCategorii.css';
import PanouAutentificare from '../../PanouAutentificare/PanouAutentificare';

function AdminCategorii(){

    let navigate= useNavigate();

    const [content,setContent]=useState([]);
    const [isLogged,setLogged]=useState(false);

    const getCategories=()=>{
        Axios.get("http://localhost:3001/categorii").then((response) => {
        setContent(response.data);
        });
    } 

    useEffect(()=>{

        const checkIfLogin=()=>{
            if(window.localStorage.getItem("token") && window.localStorage.getItem("role")==='1')
                {
                    setLogged(true);
                    getCategories();
                }
            else 
            {
                setLogged(false);
                navigate('/autentificare');
            }
        }  
        
        checkIfLogin();
    },[navigate]);

    const redirect = (ID_categorie) => {
    navigate(`/admin/categorii/${ID_categorie}`);
    }

    const redirectInsert = () => {
        navigate(`/admin/categorii/adaugareCategorie`);
    }
    
    return(
        <>
        {isLogged && (
            <div className="adminCategoriiPanou">
            <div className="adminCategoriiNavBar">
                <NavBar/>
            </div>
            <div className="adminCategoriiRow">
                <p className="pAdminCategoriiRow">CATEGORII ACTUALE</p>
                <hr className="hrCategorii"></hr>
                {content.map((val)=>{
                        return(
                            <div className="categorieChild" key={val.Cod_categorie}>
                                <p key={val.Cod_utilizator} className="pAdminCategorieRow">{val.Denumire} || codul categoriei: {val.Cod_categorie}</p>  
                                <button className="pAdminVeziDetalii" onClick={()=>{redirect(val.Cod_categorie)}}>Vizualizare detalii</button>
                            </div>
                            
                        )
                })}
            </div>
            <div className="adminCategoriiRow2">
                <p className="pAdminCategoriiInsertButton" onClick={()=>redirectInsert()}>ADAUGA CATEGORIE NOUA</p>
            </div>
        </div>  
        )}
        {
            !(isLogged) && (
        <PanouAutentificare/>)
        }
        
    </>
    )
}
export default AdminCategorii;