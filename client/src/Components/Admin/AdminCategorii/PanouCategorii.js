import  Axios  from 'axios';
import React, {useState,useEffect} from 'react';
import { useNavigate } from 'react-router-dom';
import PanouAutentificare from '../../PanouAutentificare/PanouAutentificare';
import './PanouCategorii.css';

function PanouCategorii(props){
    
    const [Cod_categorie]=useState(props.Cod_categorie);
    const [denumireNoua, setDenumireNoua]=useState(props.Denumire);
    const [descriereNoua,setDescriereNoua]=useState(props.Descriere);
    const [file, setFile] = useState();
    const [fileName, setFileName] = useState(props.Imagine);
    const [isLogged,setLogged]=useState(false);
  
    const checkIfLogin=()=>{
        if(window.localStorage.getItem("token") && window.localStorage.getItem("role")==='1')
            setLogged(true);
        else 
        {
            setLogged(false);
            navigate('/autentificare');
        }
    }   

    const saveFile = (e) => {
        setFile(e.target.files[0]);
        setFileName(e.target.files[0].name);
    };

    let navigate= useNavigate();

    const redirect = () => {
        navigate(`/admin/categorii`);
    }  

    const updateCategorie=(Cod_categorie)=>{
        const formData = new FormData();
        formData.append("file", file);
        formData.append("fileName", fileName);
        formData.append("Denumire",denumireNoua);
        formData.append("Descriere",descriereNoua);
        formData.append("Cod_categorie",Cod_categorie);
        if(denumireNoua==='' ||descriereNoua===''||fileName==='')
        alert("Completati toate campurile.");
        else {
            Axios.put("http://localhost:3001/categorii/modificareCategorie",formData).then((response) => {
                alert(response.data);
                if(response.data==="Categorie modificata cu succes.")
                window.location.reload(true);
            });
        }
    }

    const stergereCategorie=(Cod_categorie)=>{
        Axios.delete(`http://localhost:3001/categorii/stergereCategorie/${Cod_categorie}`).then((response)=>{
            alert(response.data);
            if(response.data==="Categorie stearsa cu succes.")
                redirect();
        });
    }

    useEffect(()=>{    
        checkIfLogin();
    });
    return(
        <>
            {isLogged && (
                <div className="panouCategorii">   
                    <div className="firstRowPanouCategorii">
                        <div className="firstRowPanouCategoriiFirstColumn">
                            <label className="firstRowPanouCategoriiFirstColumnLabel">Denumire: </label>
                        </div>
                        <div className="firstRowPanouCategoriiSecondColumn">
                            <textarea className="firstRowPanouCategoriiTextBoxDenumire" name="denumire" rows="1" 
                                    defaultValue={props.Denumire} onChange={(event)=>{setDenumireNoua(event.target.value);}}>
                            </textarea>
                        </div>
                    </div>
                    <div className="firstRowPanouCategorii">
                        <div className="firstRowPanouCategoriiFirstColumn">
                            <label className="firstRowPanouCategoriiFirstColumnLabel">Descriere: </label>
                        </div>
                        <div className="firstRowPanouCategoriiSecondColumn">
                            <textarea className="firstRowPanouCategoriiTextBoxDescriere" name="descriere" rows="1" 
                                    defaultValue={props.Descriere} onChange={(event)=>{setDescriereNoua(event.target.value);}}>
                            </textarea>
                        </div>
                    </div>
                    <div className="firstRowPanouCategorii">
                        <div className="firstRowPanouCategoriiFirstColumn">
                            <label className="firstRowPanouCategoriiFirstColumnLabel">Imagine: </label>
                        </div>
                        <div className="firstRowPanouCategoriiSecondColumn">
                            <img className="firstRowPanouCategoriiTextBoxImagine" 
                                src={`/images/categorii/${fileName}`} alt="error"/>
                            <br></br>
                            <label>Schimba imaginea    </label>
                            <input type="file" name="files" onChange={saveFile} />
                        </div>
                    </div>
                    <div className="secondRowPanouCategorii">
                        <div className="firstRowPanouCategoriiButton">
                            <button className="firstRowPanouCategoriiSubmitButton1" onClick={()=>{updateCategorie(Cod_categorie);}}>Editeaza categoria</button>
                            <button className="firstRowPanouCategoriiSubmitButton2" onClick={()=>{stergereCategorie(Cod_categorie);}}>Sterge categoria</button>
                        </div>
                    </div>
                </div>
            )}
            {
                !isLogged && (
                    <PanouAutentificare/>
                )
            }
        </>
        
    )
}
export default PanouCategorii;