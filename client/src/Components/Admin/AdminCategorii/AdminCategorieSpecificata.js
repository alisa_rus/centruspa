import  Axios  from 'axios';
import React, {useEffect,useState} from 'react'
import { useParams } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import NavBar from '../../NavBarAdmin/NavBar';
import PanouAutentificare from '../../PanouAutentificare/PanouAutentificare';
import PanouCategorii from './PanouCategorii';
import './PanouCategorii.css';

function AdminCategorieSpecificata(){

    let { id } = useParams();
    let navigate=useNavigate();

    const [content,setContent]=useState([]);
    const [isLogged,setLogged]=useState(false);
  
    useEffect(()=>{

        const checkIfLogin=()=>{
            if(window.localStorage.getItem("token") && window.localStorage.getItem("role")==='1')
                {
                    setLogged(true);
                    Axios.get(`http://localhost:3001/categorii/${id}`).then((response) => {
                        setContent(response.data);
                    });
                } 
            else 
            {
                setLogged(false);
                navigate('/autentificare');
            }
        }
            checkIfLogin();

    },[id,navigate])
   
    return(
        <>  {isLogged && (
                <div className="adminCategoriiPanou">
                    <div className="adminCategoriiNavBar">
                        <NavBar/>
                    </div>
                    <div className="adminCategorieSpecificataColumn">
                    <p className="panouCategorieP"> Vizualizeaza detaliile categoriei</p>  
                        {content.map((val)=>{
                            return(
                            <PanouCategorii key={val.Cod_categorie} Cod_categorie={val.Cod_categorie} Denumire={val.Denumire} Descriere={val.Descriere} Imagine={val.Imagine}/>)}       
                        )}
                    </div>
                </div>
            )}
            {
            !(isLogged) && (
            <PanouAutentificare/>)
            }
        </>
    )
}
export default  AdminCategorieSpecificata;