import React,{useState,useEffect} from 'react';
import Axios from 'axios';
import './PanouClient.css';
import { useNavigate } from 'react-router-dom';
import PanouAutentificare from '../../PanouAutentificare/PanouAutentificare';

function PanouClient(props){
    
    const [flagParola,setFlagParola]=useState(false);
    const [nume, setNume]=useState(props.Nume);
    const [prenume, setPrenume]=useState(props.Prenume);
    const [Cod_utilizator]=useState(props.Cod_utilizator);
    const [email, setEmail]=useState(props.Email);
    const [parola, setParola]=useState(props.Parola);
    const [telefon,setTelefon]=useState(props.Telefon);
    const [isLogged,setLogged]=useState(false);
    let navigate= useNavigate();

    const redirect = () => {
        navigate(`/admin/clienti`);
    }

    const modificaContClient=()=>{
        if(nume===''||prenume===''||telefon===''||email===''||parola==='')
            alert("Completati toate campurile.");
        else
        {
            Axios.put("http://localhost:3001/user/clienti/modificareClient",{
                Cod_utilizator:Cod_utilizator,
                Nume:nume,
                Prenume:prenume,
                Telefon:telefon,
                Email:email,
                Parola:parola,
                Flag_parola:flagParola,
            }
            ).then((response)=>{
                alert(response.data);
                if(response.data==='Cont client modificat cu succes.')
                    window.location.reload(true);
            })
        }
    }
    const stergeContClient=()=>{
        Axios.delete(`http://localhost:3001/user/clienti/stergereClient/${Cod_utilizator}`).then((response)=>{
            alert(response.data);
            if(response.data==='Cont client sters cu succes.')
                redirect();
        })
    }
    useEffect(()=>{

        const checkIfLogin=()=>{
            if(window.localStorage.getItem("token") && window.localStorage.getItem("role")==='1')
                setLogged(true);
            else 
            {
                setLogged(false);
                navigate('/autentificare');
            }
        }
        checkIfLogin();

    }) 
    return(
        <>
            {isLogged && (
            <div className="panouClient">   
                <div className="firstRowPanouClient">
                    <div className="firstRowPanouClientFirstColumn">
                        <label className="firstRowPanouClientFirstColumnLabel">Nume: </label>
                    </div>
                    <div className="firstRowPanouClientSecondColumn">
                        <textarea className="firstRowPanouClientTextBoxDenumire" name="denumire" rows="1" 
                                   defaultValue={props.Nume} onChange={(e)=>{setNume(e.target.value)}}>
                        </textarea>
                    </div>
                </div>
                <div className="firstRowPanouClient">
                    <div className="firstRowPanouClientFirstColumn">
                        <label className="firstRowPanouClientFirstColumnLabel">Prenume: </label>
                    </div>
                    <div className="firstRowPanouClientSecondColumn">
                        <textarea className="firstRowPanouClientTextBoxDenumire" name="denumire" rows="1" 
                                   defaultValue={props.Prenume} onChange={(e)=>{setPrenume(e.target.value)}}>
                        </textarea>
                    </div>
                </div>
                <div className="firstRowPanouClient">
                    <div className="firstRowPanouClientFirstColumn">
                        <label className="firstRowPanouClientFirstColumnLabel">Telefon: </label>
                    </div>
                    <div className="firstRowPanouClientSecondColumn">
                        <textarea className="firstRowPanouClientTextBoxDenumire" name="denumire" rows="1" 
                                   defaultValue={props.Telefon} onChange={(e)=>{setTelefon(e.target.value)}}>
                        </textarea>
                    </div>
                </div>
                <div className="firstRowPanouClient">
                    <div className="firstRowPanouClientFirstColumn">
                        <label className="firstRowPanouClientFirstColumnLabel">Email: </label>
                    </div>
                    <div className="firstRowPanouClientSecondColumn">
                        <textarea className="firstRowPanouClientTextBoxDenumire" name="denumire" rows="1" 
                                   defaultValue={props.Email} onChange={(e)=>{setEmail(e.target.value)}}>
                        </textarea>
                    </div>
                </div>
                <div className="firstRowPanouClient">
                    <div className="firstRowPanouClientFirstColumn">
                        <label className="firstRowPanouClientFirstColumnLabel">Parola: </label>
                    </div>
                    <div className="firstRowPanouClientSecondColumn">
                        <input type='password' className="firstRowPanouClientTextBoxDenumire" name="denumire" rows="1" 
                                   defaultValue={props.Parola} onChange={(e)=>{setParola(e.target.value);setFlagParola(true)}}>
                        </input>
                    </div>
                </div>
                <div className="secondRowPanouClient">
                    <div className="firstRowPanouClientButton">
                        <button className="firstRowPanouClientiSubmitButton1" onClick={modificaContClient}>Editeaza detalii cont</button>
                        <button className="firstRowPanouClientiSubmitButton2" onClick={stergeContClient}>Sterge cont client</button>
                    </div>
                </div>
            </div>
            )}
            {!isLogged && (
                <PanouAutentificare/>
            )}
        </>
        
    )
}
export default PanouClient;