import React,{useState,useEffect} from 'react'
import Axios from 'axios';
import { useNavigate } from 'react-router-dom';
import NavBar  from '../../NavBarAdmin/NavBar';
import './AdminClienti.css';
import PanouAutentificare from '../../PanouAutentificare/PanouAutentificare';

function AdminClienti(){
  
    const [content,setContent]=useState([]);
    const [isLogged,setLogged]=useState(false);

    const getClienti=()=>{
        Axios.get("http://localhost:3001/user/clienti").then((response) => {
        setContent(response.data);
        });
    }
    let navigate= useNavigate();

    const redirect = (Cod_utilizator) => {
        navigate(`/admin/clienti/${Cod_utilizator}`);
    }
    const redirectInsert = () => {
        navigate(`/admin/clienti/adaugareClient`);
    }
    
    useEffect(()=>{

        const checkIfLogin=()=>{
            if(window.localStorage.getItem("token") && window.localStorage.getItem("role")==='1')
                {
                    setLogged(true);
                    getClienti();
                }
            else 
            {
                setLogged(false);
                navigate('/autentificare');
            }
        }

        checkIfLogin();
        
    },[navigate]);

    return(
        <>
        {isLogged && (
        <div className="adminClientiPanou">
            <div className="adminClientiNavBar">
                <NavBar/>
            </div>
            <div className="adminClientiRow">
                <p className="pAdminClientiRow">CLIENTI ACTUALI</p>
                <hr className="hrClienti"></hr>
                {content.map((val)=>{
                        return(
                            <div className="clientChild" key={val.Cod_utilizator}>
                                <p key={val.Cod_utilizator} className="pAdminClientRow">{val.Nume} {val.Prenume} || {val.Email} || {val.Telefon}</p>  
                                <button className="pAdminVeziDetalii" onClick={()=>{redirect(val.Cod_utilizator)}}>Vizualizare detalii</button>
                            </div>
                            
                        )
                })}
            </div>
            <div className="adminClientiRow2">
                <p className="pAdminClientiInsertButton" onClick={()=>redirectInsert()}>ADAUGA CLIENT NOU</p>
            </div>
        </div>  
        )}
        {!isLogged && (
            <PanouAutentificare/>
        )}
            
        </>
    )
}
export default AdminClienti;