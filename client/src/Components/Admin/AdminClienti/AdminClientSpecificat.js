import  Axios  from 'axios';
import React, {useEffect,useState} from 'react'
import { useParams } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import NavBar from '../../NavBarAdmin/NavBar';
import PanouAutentificare from '../../PanouAutentificare/PanouAutentificare';
import PanouClient from './PanouClient';


function AdminClientSpecificat(){

    let { id } = useParams();
    let navigate=useNavigate();

    const [content,setContent]=useState([]);
    const [isLogged,setLogged]=useState(false);

    useEffect(()=>{

        const checkIfLogin=()=>{
            if(window.localStorage.getItem("token") && window.localStorage.getItem("role")==='1')
                {
                    setLogged(true);
                    Axios.get(`http://localhost:3001/user/clientById/${id}`).then((response) => {
                        setContent(response.data);
                    });
                }
            else 
            {
                setLogged(false);
                navigate('/autentificare');
            }
        }
        checkIfLogin();
    },[navigate,id]) 
    

    return(
        <>
            {isLogged && (
            <div className="adminClientiPanou">
                <div className="adminClientiNavBar">
                    <NavBar/>
                </div>
                <div className="adminClientSpecificatColumn">
                    <p className="panouClientP"> Vizualizeaza detaliile contului</p>  
                    {content.map((val)=>{
                        return(
                        <PanouClient key={val.Cod_utilizator} Cod_utilizator={val.Cod_utilizator} Nume={val.Nume} Prenume={val.Prenume}
                        Email={val.Email} Parola={val.Parola} Telefon={val.Telefon}
                        />)}       
                    )}
                </div>           
            </div>
            )}
            { !isLogged && (
                <PanouAutentificare/>
            )}
            
        </>
    )
}
export default AdminClientSpecificat;