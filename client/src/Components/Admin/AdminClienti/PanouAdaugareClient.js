import  Axios  from 'axios';
import React, {useState,useEffect} from 'react';
import NavBar from '../../NavBarAdmin/NavBar';
import {useNavigate} from 'react-router-dom';
import './PanouClient.css';
import PanouAutentificare from '../../PanouAutentificare/PanouAutentificare';

function PanouAdaugareClient(){

    const [numeNou,setNumeNou]=useState('');
    const [prenumeNou,setPrenumeNou]=useState('');
    const [telefonNou,setTelefonNou]=useState('');
    const [emailNou,setEmailNou]=useState('');
    const [parolaNoua,setParolaNoua]=useState('');
    const [isLogged,setLogged]=useState(false);

    const addClient=()=>{
        if(numeNou===''||prenumeNou===''||telefonNou===''||emailNou===''||parolaNoua==='')
            alert("Completati toate campurile.");
        else{
            Axios.post("http://localhost:3001/user/clienti/adaugareClient",
                {
                    Nume:numeNou,
                    Prenume:prenumeNou,
                    Email:emailNou,
                    Telefon:telefonNou,
                    Parola:parolaNoua
                }
            ).then((response)=>{
                alert(response.data);
                if(response.data==='Cont client inserat cu succes.')
                    redirect();
            });
        }
    }


    let navigate= useNavigate();

    const redirect = () => {
        navigate(`/admin/clienti`);
    }

    useEffect(()=>{

        const checkIfLogin=()=>{
            if(window.localStorage.getItem("token") && window.localStorage.getItem("role")==='1')
                setLogged(true);
            else 
            {
                setLogged(false);
                navigate('/autentificare');
            }
        }
        checkIfLogin();
    }) 
    return(
        <>
            {isLogged && (
                <div className="adminClientiPanou">
                <div className="adminClientiNavBar">
                    <NavBar/>
                </div>
                <div className="adminAdaugareClientColumn">
                    <p className="panouClientP"> Completeaza detaliile noului cont</p>   
                    <div className="firstRowPanouClient">
                        <div className="firstRowPanouClientFirstColumn">
                            <label className="firstRowPanouClientFirstColumnLabel">Nume: </label>
                        </div>
                        <div className="firstRowPanouClientSecondColumn">
                            <textarea className="firstRowPanouClientTextBoxDenumire" name="denumire" rows="1" 
                                    onChange={(e)=>{setNumeNou(e.target.value)}}>
                            </textarea>
                        </div>
                    </div>
                    <div className="firstRowPanouClient">
                        <div className="firstRowPanouClientFirstColumn">
                            <label className="firstRowPanouClientFirstColumnLabel">Prenume: </label>
                        </div>
                        <div className="firstRowPanouClientSecondColumn">
                            <textarea className="firstRowPanouClientTextBoxDenumire" name="denumire" rows="1" 
                                    onChange={(e)=>{setPrenumeNou(e.target.value)}}>
                            </textarea>
                        </div>
                    </div>
                    <div className="firstRowPanouClient">
                        <div className="firstRowPanouClientFirstColumn">
                            <label className="firstRowPanouClientFirstColumnLabel">Telefon: </label>
                        </div>
                        <div className="firstRowPanouClientSecondColumn">
                            <textarea className="firstRowPanouClientTextBoxDenumire" name="denumire" rows="1" 
                                    onChange={(e)=>{setTelefonNou(e.target.value)}}>
                            </textarea>
                        </div>
                    </div>
                    <div className="firstRowPanouClient">
                        <div className="firstRowPanouClientFirstColumn">
                            <label className="firstRowPanouClientFirstColumnLabel">Email: </label>
                        </div>
                        <div className="firstRowPanouClientSecondColumn">
                            <textarea className="firstRowPanouClientTextBoxDenumire" name="denumire" rows="1" 
                                        onChange={(e)=>{setEmailNou(e.target.value)}}>
                            </textarea>
                        </div>
                    </div>
                    <div className="firstRowPanouClient">
                        <div className="firstRowPanouClientFirstColumn">
                            <label className="firstRowPanouClientFirstColumnLabel">Parola: </label>
                        </div>
                        <div className="firstRowPanouClientSecondColumn">
                            <input type='password' className="firstRowPanouClientTextBoxDenumire" name="denumire" 
                                    onChange={(e)=>{setParolaNoua(e.target.value)}}>
                            </input>
                        </div>
                    </div>
                    <div className="secondRowPanouClientAdd">
                            <button className="firstRowPanouClientInsertButton" onClick={addClient}>Adauga cont client</button>
                    </div>
                </div>
            </div>
            )}
            {!isLogged && (
                <PanouAutentificare/>
            )}
            
        </>
    )

}
export default PanouAdaugareClient;