import  Axios  from 'axios';
import React, {useEffect,useState} from 'react'
import { useParams,useNavigate } from 'react-router-dom';
import NavBar from '../../NavBarAdmin/NavBar';
import PanouServiciu from './PanouServiciu';
import './AdminServicii.css'
import PanouAutentificare from '../../PanouAutentificare/PanouAutentificare';


function AdminServiciuSpecificat(){

    let { id } = useParams();
    let navigate= useNavigate();
    const [content,setContent]=useState([]);
    const [categoria,setCategoria]=useState('');
    const [isLogged,setLogged]=useState(false);

    useEffect(()=>{

        const checkIfLogin=()=>{
            if(window.localStorage.getItem("token") && window.localStorage.getItem("role")==='1')
                {
                    setLogged(true);       
                    Axios.get(`http://localhost:3001/servicii/${id}`).then((response) => {
                        setContent(response.data);
                    });
                }
            else 
            {
                setLogged(false);
                navigate('/autentificare');
            }
        }  
        
        checkIfLogin();        
    },[id,navigate]) 
    
    const getCategorie=((id)=>{
            Axios.get(`http://localhost:3001/categorii/${id}`).then((response) => { 
            setCategoria(response.data[0].Denumire);
        });    
    })
    return(
        <>  {
            isLogged && (
                <div className="adminServiciiPanou">
                    <div className="adminServiciiNavBar">
                        <NavBar/>
                    </div>
                    <div className="adminServiciuSpecificatColumn">
                        <p className="panouServiciuP"> Vizualizeaza detaliile serviciului</p>  
                        {content.map((val)=>{
                            getCategorie(val.Cod_categorie);
                            return(
                            <PanouServiciu key={val.Cod_serviciu} categorie={categoria} Cod_serviciu={val.Cod_serviciu} Cod_categorie={val.Cod_categorie} Denumire={val.Denumire} Descriere={val.Descriere} Imagine={val.Imagine} Beneficii={val.Beneficii} Pret={val.Pret} Durata={val.Durata}/>)}       
                        )}
                    </div>
                </div>
                )
            }
            {
                !isLogged && (
                    <PanouAutentificare/>
                )
            }
           
        </>
    )
}
export default AdminServiciuSpecificat;