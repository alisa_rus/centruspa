import React,{useState,useEffect} from 'react';
import Axios from 'axios';
import { useNavigate } from 'react-router-dom';
import NavBar  from '../../NavBarAdmin/NavBar';
import './AdminServicii.css';
import PanouAutentificare from '../../PanouAutentificare/PanouAutentificare';

function AdminServicii(){

    const [content,setContent]=useState([]);
    let navigate= useNavigate();
    const [isLogged,setLogged]=useState(false);

    const redirect = (ID_serviciu) => {
        navigate(`/admin/servicii/${ID_serviciu}`);
    }

    const redirectToInsert = () => {
        navigate(`/admin/servicii/adaugareServiciu`);
    }

    const getServicii=()=>{
        Axios.get("http://localhost:3001/servicii").then((response) => {
        setContent(response.data);
        });
    }
    
    useEffect(()=>{

        const checkIfLogin=()=>{
            if(window.localStorage.getItem("token") && window.localStorage.getItem("role")==='1')
                {
                    setLogged(true);
                    getServicii();
                }
            else 
            {
                setLogged(false);
                navigate('/autentificare');
            }
        }
        checkIfLogin();
        
    },[navigate]);

    return(
        <>
            {isLogged && (
            <div className="adminServiciiPanou">
                <div className="adminServiciiNavBar">
                    <NavBar/>
                </div>
                <div className="adminServiciiRow">
                    <p className="pAdminServiciiRow">SERVICIILE ACTUALE</p>
                    <hr className="hrServicii"></hr>
                    {content.map((val)=>{
                        return(
                            <div className="serviciuChild" key={val.Cod_serviciu}>
                                <p key={val.Cod_serviciu} className="pAdminServiciuRow">{val.Denumire} || Pret: {val.Pret} lei || Durata: {val.Durata} minute</p>  
                                <button className="pAdminVeziDetalii" onClick={()=>{redirect(val.Cod_serviciu)}}>Vizualizare detalii</button>
                            </div>
                        )
                    })}
                </div>
                <div className="adminServiciiRow2">
                    <p className="pAdminServiciiInsertButton" onClick={()=>redirectToInsert()}>ADAUGA SERVICIU NOU</p>
                </div>
            </div>
            )}
            {!isLogged &&
            <PanouAutentificare/> 
            }
        </>
    )
}
export default AdminServicii;