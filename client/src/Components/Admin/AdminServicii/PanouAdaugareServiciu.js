import React, { useState,useEffect } from 'react';
import  Axios  from 'axios';
import NavBar from '../../NavBarAdmin/NavBar';
import {useNavigate} from 'react-router-dom';
import PanouAutentificare from '../../PanouAutentificare/PanouAutentificare';

function PanouAdaugareServiciu(){

    const [denumire, setDenumire]=useState('');
    const [categorii,setCategorii]=useState([]);
    const [descriere,setDescriere]=useState('');
    const [beneficii,setBeneficii]=useState('');
    const [pret,setPret]=useState('');
    const [durata,setDurata]=useState('');
    const [Cod_categorie,setCodCategorie]=useState('');
    const [ready,setReady]=useState(false);
    const [file, setFile] = useState();
    const [fileName, setFileName] = useState("");
    const [isLogged,setLogged]=useState(false);

    const saveFile = (e) => {
        setFile(e.target.files[0]);
        setFileName(e.target.files[0].name);
    };
    let navigate= useNavigate();

    const redirect = () => {
        navigate(`/admin/servicii`);
    }

    const adaugareServiciu= async(e)=>{
        const formData = new FormData();
        formData.append("file", file);
        formData.append("fileName", fileName);
        formData.append("Denumire",denumire);
        formData.append("Descriere",descriere);
        formData.append("Beneficii",beneficii);
        formData.append("Pret",pret);
        formData.append("Durata",durata);
        if(Cod_categorie==='') formData.append("Cod_categorie",categorii[0].Cod_categorie);
        else formData.append("Cod_categorie",Cod_categorie);
        if(denumire===''||descriere===''||beneficii===''||pret===''||durata===''||fileName==='')
            alert('Completati toate campurile');
        else {
            Axios.post("http://localhost:3001/servicii/adaugareServiciu",formData).then((response) => {
                    alert(response.data);
                    if(response.data==="Serviciu inserat cu succes.")
                        redirect();
                });
            }
    }

    useEffect(()=>{
        const checkIfLogin=()=>{
            if(window.localStorage.getItem("token") && window.localStorage.getItem("role")==='1')
                {
                    setLogged(true);       
                    Axios.get(`http://localhost:3001/categorii/`).then((response) => {
                        setCategorii(response.data);
                        setReady(true); 
                    });
                }
            else 
            {
                setLogged(false);
                navigate('/autentificare');
            }
        }  
        checkIfLogin();
    },[navigate])

    return(
        <>  {isLogged && (
            <div className="adminServiciiPanou">
                <div className="adminServiciiNavBar">
                    <NavBar/>
                </div>
                <div className="adminAdaugareServiciuColumn">
                <p className="panouServiciuP"> Completeaza detaliile noului serviciu</p>         
                        <div className="firstRowPanouServicii">
                            <div className="firstRowPanouServiciiFirstColumn">
                                <label className="firstRowPanouServiciiFirstColumnLabel">Denumire: </label>
                            </div>
                            <div className="firstRowPanouServiciiSecondColumn">
                                <textarea className="firstRowPanouServiciiTextBoxDenumire" name="denumire" rows="1" 
                                    onChange={(event)=>{setDenumire(event.target.value);}}>
                                </textarea>
                            </div>
                        </div>
                        <div className="firstRowPanouServicii">
                            <div className="firstRowPanouServiciiFirstColumn">
                                <label className="firstRowPanouServiciiFirstColumnLabel">Categorie: </label>
                            </div>
                            <div className="firstRowPanouServiciiSecondColumn">
                                {ready && (
                                    <select className="selectServicii" defaultValue={categorii[0].Denumire} onChange={(event)=>{setCodCategorie(event.target.value);}}>
                                    {categorii.map((option) => (
                                    <option key={option.Cod_categorie} value={option.Cod_categorie}>{option.Denumire}</option>
                                    ))}
                                </select>
                                )}
                            
                            </div>               
                        </div>
                        <div className="firstRowPanouServicii">
                            <div className="firstRowPanouServiciiFirstColumn">
                                <label className="firstRowPanouServiciiFirstColumnLabel">Descriere: </label>
                            </div>
                            <div className="firstRowPanouServiciiSecondColumn">
                                <textarea className="firstRowPanouServiciiTextBoxDescriere" name="descriere" rows="1"
                                    onChange={(event)=>{setDescriere(event.target.value);}}>
                                </textarea>
                            </div>
                        </div>
                        <div className="firstRowPanouServicii">
                            <div className="firstRowPanouServiciiFirstColumn">
                                <label className="firstRowPanouServiciiFirstColumnLabel">Beneficii: </label>
                            </div>
                            <div className="firstRowPanouServiciiSecondColumn">
                                <textarea className="firstRowPanouServiciiTextBoxDescriere" name="descriere" rows="1"
                                    onChange={(event)=>{setBeneficii(event.target.value);}}>
                                </textarea>
                            </div>
                        </div>
                        <div className="firstRowPanouServicii">
                            <div className="firstRowPanouServiciiFirstColumn">
                                <label className="firstRowPanouServiciiFirstColumnLabel">Pret: </label>
                            </div>
                            <div className="firstRowPanouServiciiSecondColumn">
                                <textarea className="firstRowPanouServiciiTextBoxDenumire" name="denumire" rows="1"
                                    onChange={(event)=>{setPret(event.target.value);}}>
                                </textarea>
                            </div>
                        </div>
                        <div className="firstRowPanouServicii">
                            <div className="firstRowPanouServiciiFirstColumn">
                                <label className="firstRowPanouServiciiFirstColumnLabel">Durata: </label>
                            </div>
                            <div className="firstRowPanouServiciiSecondColumn">
                                <textarea className="firstRowPanouServiciiTextBoxDenumire" name="denumire" rows="1"
                                    onChange={(event)=>{setDurata(event.target.value);}}>
                                </textarea>
                            </div>
                        </div>
                        <div className="firstRowPanouServicii">
                            <div className="firstRowPanouServiciiFirstColumn">
                                <label className="firstRowPanouServiciiFirstColumnLabel">Imagine: </label>
                            </div>
                            <div className="firstRowPanouServiciiSecondColumn">
                                <input type="file" onChange={saveFile} />
                            </div>
                        </div>
                        <div className="secondRowPanouServiciuAdd">
                            <button className="firstRowPanouClientInsertButton" onClick={adaugareServiciu}>Adauga serviciu nou</button>
                        </div>
                </div>
            </div>
            )}
            {
                !isLogged && (
                    <PanouAutentificare/>
                )
            }
            
        </>
    )
}
export default PanouAdaugareServiciu;