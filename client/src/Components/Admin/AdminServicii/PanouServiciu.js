import React,{useState,useEffect} from 'react';
import Axios from 'axios';
import { useNavigate } from 'react-router-dom';
import './PanouServicii.css';
import PanouAutentificare from '../../PanouAutentificare/PanouAutentificare';

function PanouServicii(props){
    
    const [denumireNoua, setDenumireNoua]=useState(props.Denumire);
    const [Cod_serviciu]=useState(props.Cod_serviciu)
    const [descriereNoua,setDescriereNoua]=useState(props.Descriere);
    const [beneficiiNoi,setBeneficiiNoi]=useState(props.Beneficii);
    const [pretNou,setPretNou]=useState(props.Pret);
    const [durataNoua]=useState(props.Durata);
    const [file, setFile] = useState();
    const [fileName, setFileName] = useState(props.Imagine);
    const [isLogged,setLogged]=useState(false);

    const saveFile = (e) => {
        setFile(e.target.files[0]);
        setFileName(e.target.files[0].name);
    };
    let navigate= useNavigate();

    const redirect = () => {
        navigate(`/admin/servicii`);
    }  

    const modificareServiciu=()=>{
        const formData = new FormData();
        formData.append("file", file);
        formData.append("fileName", fileName);
        formData.append("Denumire",denumireNoua);
        formData.append("Descriere",descriereNoua);
        formData.append("Beneficii",beneficiiNoi);
        formData.append("Pret",pretNou);
        formData.append("Durata",durataNoua);
        formData.append("Cod_serviciu",Cod_serviciu);
        if(denumireNoua==='' ||descriereNoua===''||beneficiiNoi===''||pretNou===''||durataNoua===''||fileName==='')
        alert("Completati toate campurile.");
        else {
            Axios.put("http://localhost:3001/servicii/modificareServiciu",formData).then((response) =>
            {
                alert(response.data);
                if(response.data==="Serviciu modificat cu succes.")
                window.location.reload(true);
            });
        }
    }

    const stergereServiciu=(Cod_serviciu)=>{
        Axios.delete(`http://localhost:3001/servicii/stergereServiciu/${Cod_serviciu}`).then((response)=>{
            alert(response.data);
            if(response.data==="Serviciu sters cu succes.")
                redirect();
        });
    }

    useEffect(()=>{

        const checkIfLogin=()=>{
            if(window.localStorage.getItem("token"))
                {if(window.localStorage.getItem("role")==='1')
                        setLogged(true);
                    else 
                        setLogged(false);
                }
            else setLogged(false);
        }
        checkIfLogin();

    },[])
    return(
        <>
            {isLogged && (
            <div className="panouServicii">   
                <div className="firstRowPanouServicii1">
                    <div className="firstRowPanouServiciiFirstColumn">
                        <label className="firstRowPanouServiciiFirstColumnLabel">Denumire: </label>
                    </div>
                    <div className="firstRowPanouServiciiSecondColumn">
                        <textarea className="firstRowPanouServiciiTextBoxDenumire" name="denumire" rows="1" 
                                   defaultValue={props.Denumire} onChange={(e)=>{setDenumireNoua(e.target.value)}}>
                        </textarea>
                    </div>
                </div>
                <div className="firstRowPanouServicii">
                    <div className="firstRowPanouServiciiFirstColumn">
                        <label className="firstRowPanouServiciiFirstColumnLabel">Imagine: </label>
                    </div>
                    <div className="firstRowPanouServiciiSecondColumn">
                        <img className="firstRowPanouServiciiTextBoxImagine" src={`/images/servicii/${fileName}`} alt="error"/>
                        <br></br>
                        <label>Schimba imaginea    </label>
                        <input type="file" name="files" onChange={saveFile} />
                    </div>
                </div>
                <div className="firstRowPanouServicii">
                    <div className="firstRowPanouServiciiFirstColumn">
                        <label className="firstRowPanouServiciiFirstColumnLabel">Categorie: </label>
                    </div>
                    <div className="firstRowPanouServiciiSecondColumn">
                        <textarea className="firstRowPanouServiciiTextBoxDenumire" name="denumire" rows="1"  readOnly
                                   defaultValue={props.categorie}>
                        </textarea>
                    </div>
                 
                </div>
                <div className="firstRowPanouServicii">
                    <div className="firstRowPanouServiciiFirstColumn">
                        <label className="firstRowPanouServiciiFirstColumnLabel">Descriere: </label>
                    </div>
                    <div className="firstRowPanouServiciiSecondColumn">
                        <textarea className="firstRowPanouServiciiTextBoxDescriere" name="descriere" rows="1" 
                                  defaultValue={props.Descriere} onChange={(e)=>{setDescriereNoua(e.target.value)}}>
                        </textarea>
                    </div>
                </div>
                <div className="firstRowPanouServicii">
                    <div className="firstRowPanouServiciiFirstColumn">
                        <label className="firstRowPanouServiciiFirstColumnLabel">Beneficii: </label>
                    </div>
                    <div className="firstRowPanouServiciiSecondColumn">
                        <textarea className="firstRowPanouServiciiTextBoxDescriere" name="descriere" rows="1" 
                                  defaultValue={props.Beneficii} onChange={(e)=>{setBeneficiiNoi(e.target.value)}}>
                        </textarea>
                    </div>
                </div>
                <div className="firstRowPanouServicii">
                    <div className="firstRowPanouServiciiFirstColumn">
                        <label className="firstRowPanouServiciiFirstColumnLabel">Pret: </label>
                    </div>
                    <div className="firstRowPanouServiciiSecondColumn">
                        <textarea className="firstRowPanouServiciiTextBoxDenumire" name="denumire" rows="1" 
                                   defaultValue={props.Pret} onChange={(e)=>{setPretNou(e.target.value)}}>
                        </textarea>
                    </div>
                </div>
                <div className="firstRowPanouServicii">
                    <div className="firstRowPanouServiciiFirstColumn">
                        <label className="firstRowPanouServiciiFirstColumnLabel">Durata: </label>
                    </div>
                    <div className="firstRowPanouServiciiSecondColumn">
                        <textarea className="firstRowPanouServiciiTextBoxDenumire" name="denumire" rows="1" readOnly 
                                   defaultValue={props.Durata}>
                        </textarea>
                    </div>
                </div>
                
                <div className="secondRowPanouServicii">
                    <div className="firstRowPanouServiciiButton">
                        <button className="firstRowPanouServiciiSubmitButton1" onClick={modificareServiciu} >Editeaza serviciu</button>
                        <button className="firstRowPanouServiciiSubmitButton2" onClick={()=>{stergereServiciu(Cod_serviciu)}}>Sterge serviciu</button>
                    </div>
                </div>
            </div>
            )}
            {!isLogged && (
                <PanouAutentificare/>
            )}
        </>
        
    )
}
export default PanouServicii;