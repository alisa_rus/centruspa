import React,{useState,useEffect} from 'react';
import { useNavigate } from 'react-router-dom';
import Axios from 'axios';
import NavBar  from '../../NavBarAdmin/NavBar';

import './AdminProgramari.css';
import PanouAutentificare from '../../PanouAutentificare/PanouAutentificare';

function AdminProgramari(){

    let navigate=useNavigate();

    const [content,setContent]=useState([]);
    const [angajat,setAngajat]=useState([]);
    const [client,setClient]=useState([]);
    const [serviciu,setServiciu]=useState([]);
    const [modal, setModal] = useState(false);
    const [isLogged,setLogged]=useState(false);

    const getProgramari=()=>{
        Axios.get("http://localhost:3001/programari").then((response) => {
        setContent(response.data);
        });
    }
    const toggleModal = (idAngajat,idClient,idServiciu) => {
        setModal(true);
        getAngajat(idAngajat);
        getClient(idClient);
        getServiciu(idServiciu);
    };
    const getAngajat=(id)=>{
        Axios.get(`http://localhost:3001/user/angajatById/${id}`).then((response)=>{
            setAngajat(response.data); 
        });        
    }
    const getClient=(id)=>{
        Axios.get(`http://localhost:3001/user/clientById/${id}`).then((response)=>{
            setClient(response.data); 
        });        
    }
    const getServiciu=(id)=>{
        Axios.get(`http://localhost:3001/servicii/${id}`).then((response)=>{
            setServiciu(response.data); 
        });        
    }
    const sendEmail=(id,zi,ora)=>{
        Axios.get(`http://localhost:3001/user/clientById/${id}`).then((response)=>{
            console.log(response.data[0].Email);
            Axios.post("http://localhost:3001/sendEmail/send",{
                "destinatar":response.data[0].Email,
                "textEmail":"Test: Programarea din data de "+zi+" de la ora "+ora.substr(0,5)+" a fost anulata. Multumim pentru intelegere."
            }).then((response)=>{
                if(response.data==='Email trimis.')
                    window.location.reload(true);
            }); 
        });      
    }
    const stergereProgramare=(id,Cod_client,zi,ora)=>{
        Axios.delete(`http://localhost:3001/programari/stergereProgramare/${id}`).then((response)=>{
            alert(response.data);
            if(response.data==='Programare stearsa cu succes.')
                {
                    sendEmail(Cod_client,zi,ora);
                    
                }
        });  
    }

    useEffect(()=>{
        
        const checkIfLogin=()=>{
            if(window.localStorage.getItem("token") && window.localStorage.getItem("role")==='1')
                {
                    setLogged(true);
                    getProgramari();
                }
            else 
            {
                setLogged(false);
                navigate('/autentificare');
            }
        }
        checkIfLogin();
   
    },[navigate]);

    return(
        <> {isLogged && (
            <div className="adminProgramariPanou">
                <div className="adminProgramariNavBar">
                    <NavBar/>
                </div>
                <div className="adminProgramari">
                    <div className="adminProgramariRow">
                        <p className="pAdminProgramariRow">PROGRAMARI EXISTENTE</p>
                        <div className="adminProgramariRow2">
                            <table className="tabelProgramari">
                                <thead>
                                    <tr className="randTabelProgramari">
                                        <th className="hTabelProgramari">Cod client</th>
                                        <th className="hTabelProgramari">Cod angajat</th>
                                        <th className="hTabelProgramari">Cod serviciu</th>
                                        <th className="hTabelProgramari">Status</th>
                                        <th className="hTabelProgramari">Data</th>
                                        <th className="hTabelProgramari">Ora</th>
                                        <th className="hTabelProgramari">Pret</th>
                                        <th className="hTabelProgramari">Detalii programare</th>
                                        <th className="hTabelProgramari">Sterge programare</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    {content.map((val)=>{
                                        let x=val.Zi;
                                        let y=val.OraStart;            
                                    return(                       
                                            <tr className="randDateTabelProgramari" key={val.Cod_programare}>
                                                <td>{val.Cod_client}</td>
                                                <td>{val.Cod_angajat}</td>
                                                <td>{val.Cod_serviciu}</td>
                                                <td>{val.Status}</td>
                                                <td>{x.substr(0,10)}</td>
                                                <td>{y.substr(0,5)}</td>
                                                <td>{val.Pret} lei</td>
                                                <td>
                                                    <button className="tdButonProgramariButton" onClick={()=>{toggleModal(val.Cod_angajat,val.Cod_client,val.Cod_serviciu)}}>DETALII</button>
                                                </td>
                                                <td>
                                                    <button className="tdButonProgramariButton" onClick={()=>{stergereProgramare(val.Cod_programare,val.Cod_client,x.substr(0,10),val.OraStart)}}>STERGE</button>
                                                </td>
                                            </tr>
                                                        
                                    )
                                    })}
                                </tbody>
                            
                            </table>
                            {modal && (
                                <div className="modalProgramari">
                                    <div className="modalProgramariRow">
                                        <p className="pModalProgramari">DETALII PROGRAMARE</p>
                                        <p className="closeModalProgramari" onClick={()=>{setModal(false)}}>X</p>
                                    </div>
                                    <div className="modalProgramariSecondRow">
                                        {angajat.map((val)=>{
                                            return(
                                                <p key={val.Cod_utilizator}><b>Angajat:</b> {val.Nume} {val.Prenume}</p>
                                            )            
                                        })}
                                        {client.map((val)=>{
                                            return(
                                                <p key={val.Cod_utilizator}><b>Client:</b> {val.Nume} {val.Prenume}</p>
                                            )            
                                        })}
                                        {serviciu.map((val)=>{
                                            return(
                                                <p key={val.Cod_serviciu}><b>Serviciu:</b> {val.Denumire}</p>
                                            )            
                                        })}
                                    </div>
                                </div>
                            )}
                        </div>                   
                    </div>
                </div>
            </div>          
            )}
            {!isLogged && (
                <PanouAutentificare/>
            )}
            
        </>
    )
}
export default AdminProgramari;