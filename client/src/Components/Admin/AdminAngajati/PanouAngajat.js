import React,{useState,useEffect} from "react";
import Axios from 'axios';
import { useNavigate } from 'react-router-dom';
import './PanouAngajat.css';
import FormData from 'form-data'
import PanouAutentificare from "../../PanouAutentificare/PanouAutentificare";

function PanouAngajat(props){

    const [flagParola,setFlagParola]=useState(false);
    const [nume, setNume]=useState(props.Nume);
    const [prenume, setPrenume]=useState(props.Prenume);
    const [Cod_utilizator]=useState(props.Cod_utilizator);
    const [email, setEmail]=useState(props.Email);
    const [parola, setParola]=useState(props.Parola);
    const [telefon,setTelefon]=useState(props.Telefon);
    const [descriere,setDescriere]=useState(props.Descriere);
    const [file, setFile] = useState();
    const [fileName, setFileName] = useState(props.Imagine);
    const [isLogged,setLogged]=useState(false);

    let navigate= useNavigate();

    const redirect = () => {
        navigate(`/admin/angajati`);
    }  
    const saveFile = (e) => {
        setFile(e.target.files[0]);
        setFileName(e.target.files[0].name);
    };

    const adaugaPoza=()=>{
         
        const formData = new FormData();
        formData.append("file", file);
        formData.append("Imagine", fileName);
        formData.append("Nume",nume);
        formData.append("Prenume",prenume);
        formData.append("Telefon",telefon);
        formData.append("Email",email);
        formData.append("Parola",parola);
        formData.append("Descriere",descriere);
        formData.append("Flag_parola",flagParola);

        if(nume===''||prenume===''||email===''||parola===''||telefon===''||descriere===''||fileName==='')
        alert("Completati toate campurile.");
        else {
            Axios.put("http://localhost:3001/user/angajat/modificareAngajat",formData).then((response) =>
            {
                if(response.data==="Cont angajat modificat cu succes.")
                window.location.reload(true);
            });
        }
    }

    const modificareAngajat=()=>{
        
        if(nume===''||prenume===''||email===''||parola===''||telefon===''||descriere===''||fileName==='')
            alert("Completati toate campurile.");
        else {
            Axios.put("http://localhost:3001/user/angajat/modificareAngajat",{
                "file":file,
                "Imagine":fileName,
                "Nume":nume,
                "Prenume":prenume,
                "Telefon":telefon,
                "Email":email,
                "Parola":parola,
                "Descriere":descriere,
                "Cod_utilizator":Cod_utilizator,
                "Flag_parola":flagParola,
            }).then((response) =>
            {
                alert(response.data);
                if(response.data==="Cont angajat modificat cu succes."){
                    adaugaPoza();                
                }              
            });
        }       
    }

    const stergereAngajat=()=>{
        let id=Cod_utilizator;
        Axios.delete(`http://localhost:3001/user/angajat/stergereAngajat/${id}`).then((response)=>{
            alert(response.data);
            if(response.data==="Cont angajat sters cu succes.")
            redirect();
            
        })
    }
    useEffect(()=>{

        const checkIfLogin=()=>{
            if(window.localStorage.getItem("token") && window.localStorage.getItem("role")==='1')
                setLogged(true);
            else 
            {
                setLogged(false);
                navigate('/autentificare');
            }
        }
        checkIfLogin();

    })
    return(
        <>
            {isLogged && (
                <div className="panouAngajat">   
                <div className="firstRowPanouAngajat">
                    <div className="firstRowPanouAngajatFirstColumn">
                        <label className="firstRowPanouAngajatFirstColumnLabel">Nume: </label>
                    </div>
                    <div className="firstRowPanouAngajatSecondColumn">
                        <textarea className="firstRowPanouAngajatTextBoxDenumire" name="denumire" rows="1" 
                                   defaultValue={props.Nume} onChange={(e)=>{setNume(e.target.value)}}>
                        </textarea>
                    </div>
                </div>
                <div className="firstRowPanouAngajat">
                    <div className="firstRowPanouAngajatFirstColumn">
                        <label className="firstRowPanouAngajatFirstColumnLabel">Prenume: </label>
                    </div>
                    <div className="firstRowPanouAngajatSecondColumn">
                        <textarea className="firstRowPanouAngajatTextBoxDenumire" name="denumire" rows="1" 
                                   defaultValue={props.Prenume} onChange={(e)=>{setPrenume(e.target.value)}}>
                        </textarea>
                    </div>
                </div>
                <div className="firstRowPanouAngajat">
                    <div className="firstRowPanouAngajatFirstColumn">
                        <label className="firstRowPanouAngajatFirstColumnLabel">Imagine: </label>
                    </div>
                    <div className="firstRowPanouAngajatSecondColumn">  
                        <img className="firstRowPanouAngajatTextBoxImagine" src={`/images/echipa/${fileName}`} alt="error"/>
                            <br></br>
                            <label>Schimba imaginea    </label>
                            <input type="file" name="files" onChange={saveFile} />
                    </div>
                </div>
                <div className="firstRowPanouAngajat">
                    <div className="firstRowPanouAngajatFirstColumn">
                        <label className="firstRowPanouAngajatFirstColumnLabel">Telefon: </label>
                    </div>
                    <div className="firstRowPanouAngajatSecondColumn">
                        <textarea className="firstRowPanouAngajatTextBoxDenumire" name="denumire" rows="1" 
                                   defaultValue={props.Telefon} onChange={(e)=>{setTelefon(e.target.value)}}>
                        </textarea>
                    </div>
                </div>
                <div className="firstRowPanouAngajat">
                    <div className="firstRowPanouAngajatFirstColumn">
                        <label className="firstRowPanouAngajatFirstColumnLabel">Email: </label>
                    </div>
                    <div className="firstRowPanouAngajatSecondColumn">
                        <textarea className="firstRowPanouAngajatTextBoxDenumire" name="denumire" rows="1" 
                                   defaultValue={props.Email} onChange={(e)=>{setEmail(e.target.value)}}>
                        </textarea>
                    </div>
                </div>
                <div className="firstRowPanouAngajat">
                    <div className="firstRowPanouAngajatFirstColumn">
                        <label className="firstRowPanouAngajatFirstColumnLabel">Parola: </label>
                    </div>
                    <div className="firstRowPanouAngajatSecondColumn">
                        <input type='password' className="firstRowPanouAngajatTextBoxDenumire" name="denumire" rows="1" 
                                   defaultValue={props.Parola} onChange={(e)=>{setParola(e.target.value);setFlagParola(true);}}>
                        </input>
                    </div>
                </div>
                <div className="firstRowPanouAngajat">
                    <div className="firstRowPanouAngajatFirstColumn">
                        <label className="firstRowPanouAngajatFirstColumnLabel">Descriere: </label>
                    </div>
                    <div className="firstRowPanouAngajatSecondColumn">
                        <textarea className="firstRowPanouAngajatTextBoxDescriere" name="denumire" rows="1" 
                                   defaultValue={props.Descriere} onChange={(e)=>{setDescriere(e.target.value)}}>
                        </textarea>
                    </div>
                </div>
                <div className="secondRowPanouAngajat">
                    <div className="firstRowPanouAngajatButton">
                        <button className="firstRowPanouAngajatiSubmitButton1" onClick={modificareAngajat}>Editeaza detalii cont</button>
                        <button className="firstRowPanouAngajatiSubmitButton2" onClick={stergereAngajat}>Sterge cont angajat</button>
                    </div>
                </div>
        </div>
        )}
        {!isLogged && (
            <PanouAutentificare/>
        )}
        </>
        
    )
}
export default PanouAngajat;