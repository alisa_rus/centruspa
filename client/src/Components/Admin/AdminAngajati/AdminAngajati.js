import React,{useState,useEffect} from 'react';
import Axios from 'axios';
import NavBar  from '../../NavBarAdmin/NavBar';
import { useNavigate } from 'react-router-dom';
import './AdminAngajati.css';
import PanouAutentificare from '../../PanouAutentificare/PanouAutentificare';

function AdminAngajati(){

    const [content,setContent]=useState([]);
    const [isLogged,setLogged]=useState(false);

    const getAngajati=()=>{
        Axios.get("http://localhost:3001/user/echipa").then((response) => {
        setContent(response.data);
        });
    }

    let navigate=useNavigate();

    const redirect = (Cod_utilizator) => {
        navigate(`/admin/angajati/${Cod_utilizator}`);
    } 

    const redirectToInsert = () => {
        navigate("/admin/angajati/adaugareAngajat");
    }

    useEffect(()=>{

        const checkIfLogin=()=>{
            if(window.localStorage.getItem("token") && window.localStorage.getItem("role")==='1')
                {
                    setLogged(true);
                    getAngajati();
                }
            else 
            {
                setLogged(false);
                navigate('/autentificare');
            }
        }
        checkIfLogin();
    },[navigate]);

    return(
        <> {isLogged && (
            <div className="adminAngajatiPanou">
                <div className="adminAngajatiNavBar">
                    <NavBar/>
                </div>
                <div className="adminAngajatiRow">
                    <p className="pAdminAngajatiRow">ANGAJATI ACTUALI</p>
                    <hr className="hrAngajati"></hr>
                    {content.map((val)=>{
                            return(
                                <div className="angajatChild" key={val.Cod_utilizator}>
                                    <p key={val.Cod_utilizator} className="pAdminAngajatRow">{val.Nume} {val.Prenume} || {val.Email} || {val.Telefon}</p>  
                                    <button className="pAdminVeziDetalii" onClick={()=>{redirect(val.Cod_utilizator)}}>Vizualizare detalii</button>
                                </div>
                                
                            )
                    })}
                </div>
                <div className="adminAngajatiRow2">
                    <p className="pAdminAngajatiInsertButton" onClick={redirectToInsert}>ADAUGA ANGAJAT NOU</p>
                </div>
            </div>
            )}
            {!isLogged && (
                <PanouAutentificare/>
            )}
            
        </>
    )
}
export default AdminAngajati;