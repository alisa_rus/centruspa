import Axios from 'axios';
import React,{useEffect, useState} from 'react';
import { useNavigate } from 'react-router-dom';
import NavBar from '../../NavBarAdmin/NavBar';
import Checkbox from "react-custom-checkbox";
import PanouAutentificare from '../../PanouAutentificare/PanouAutentificare';
 
function PanouAdaugareAngajat(){

    let navigate=useNavigate();
    
    const [nume, setNume]=useState('');
    const [prenume, setPrenume]=useState('');
    const [email, setEmail]=useState('');
    const [parola, setParola]=useState('');
    const [telefon,setTelefon]=useState('');
    const [descriere,setDescriere]=useState('');
    const [file, setFile] = useState();
    const [fileName, setFileName] = useState('');
    const [servicii,setServicii]=useState([]);
    const [ready,setReady]=useState(false);
    const [serviciiAngajat,setServiciiAngajat]=useState([]);
    const [isLogged,setLogged]=useState(false);

    const saveFile = (e) => {
        setFile(e.target.files[0]);
        setFileName(e.target.files[0].name);
    };
    const getServicii=()=>{
        Axios.get("http://localhost:3001/servicii/").then((response)=>{
            setServicii(response.data);
        })  
    }

    function arrayRemove(arr, value) { 
    
        return arr.filter(function(ele){ 
            return ele !== value; 
        });
    }
    const adaugaPoza=()=>{
         
        const formData = new FormData();
        formData.append("file", file);
        formData.append("Imagine", fileName);
        formData.append("Nume",nume);
        formData.append("Prenume",prenume);
        formData.append("Telefon",telefon);
        formData.append("Email",email);
        formData.append("Parola",parola);
        formData.append("Descriere",descriere)

        if(nume===''||prenume===''||email===''||parola===''||telefon===''||descriere===''||fileName==='')
        alert("Completati toate campurile.");
        else {
            Axios.put("http://localhost:3001/user/angajat/modificareAngajat",formData).then((response) =>
            {
                if(response.data==="Cont angajat modificat cu succes.")
                window.location.reload(true);
            });
        }
    }

    const adaugareAngajat=()=>{

        if(nume===''||prenume===''||email===''||parola===''||telefon===''||descriere===''||fileName===''||serviciiAngajat.length===0)
            alert("Completati toate campurile.");
        else {
            Axios.post("http://localhost:3001/user/adaugareAngajat",{  
            "Nume":nume,
            "Prenume":prenume,
            "Telefon":telefon,
            "Email":email,
            "Parola":parola,
            "Imagine":fileName,
            "Descriere":descriere,
            "Servicii":serviciiAngajat
            }).then((response)=>{
                alert(response.data);
                if(response.data==='Angajat inserat cu succes.')
                    {adaugaPoza()
                    }
            });
        }

    }

    useEffect(()=>{

        const checkIfLogin=()=>{
            if(window.localStorage.getItem("token") && window.localStorage.getItem("role")==='1')
                {
                    setLogged(true);
                    getServicii();
                }
            else 
            {
                setLogged(false);
                
                navigate('/autentificare');
            }
        }
        checkIfLogin();
        setReady(true);
        
    },[navigate])
    return(
        <>
            {isLogged && (
            <div className="adminAngajatiPanou">
                <div className="adminAngajatiNavBar">
                    <NavBar/>
                </div>
                <div className="panouAngajatInsert"> 
                        <p className="panouAngajatP"> Completeaza detaliile noului cont</p>
                        <div className="firstRowPanouAngajat">
                            <div className="firstRowPanouAngajatFirstColumn">
                                <label className="firstRowPanouAngajatFirstColumnLabel">Nume: </label>
                            </div>
                            <div className="firstRowPanouAngajatSecondColumn">
                                <textarea className="firstRowPanouAngajatTextBoxDenumire" name="denumire" rows="1" 
                                            onChange={(e)=>{setNume(e.target.value)}}>
                                </textarea>
                            </div>
                        </div>
                        <div className="firstRowPanouAngajat">
                            <div className="firstRowPanouAngajatFirstColumn">
                                <label className="firstRowPanouAngajatFirstColumnLabel">Prenume: </label>
                            </div>
                            <div className="firstRowPanouAngajatSecondColumn">
                                <textarea className="firstRowPanouAngajatTextBoxDenumire" name="denumire" rows="1" 
                                            onChange={(e)=>{setPrenume(e.target.value)}}>
                                </textarea>
                            </div>
                        </div>
                        <div className="firstRowPanouAngajat">
                            <div className="firstRowPanouAngajatFirstColumn">
                                <label className="firstRowPanouAngajatFirstColumnLabel">Imagine: </label>
                            </div>
                            <div className="firstRowPanouAngajatSecondColumn">  
                                    <input type="file" name="files" onChange={saveFile} className='inputFile' />
                            </div>
                        </div>
                        <div className="firstRowPanouAngajat">
                            <div className="firstRowPanouAngajatFirstColumn">
                                <label className="firstRowPanouAngajatFirstColumnLabel">Servicii: </label>
                            </div>
                            <div className="firstRowPanouAngajatSecondColumnAddServicii">
                                {ready &&
                                    (   
                                        servicii.map((option) => {
                                            return(
                                                <Checkbox
                                                key={option.Cod_serviciu}
                                                checked={false}
                                                onChange={(value) => {
                                                if(value===true)
                                                    serviciiAngajat.push(option.Denumire);
                                                if(value===false)
                                                    setServiciiAngajat(arrayRemove(serviciiAngajat,option.Denumire))
                                                }}
                                                label={option.Denumire}
                                              />
                                            )
                                        })
                                                 
                                    )
                                }
                            </div>
                        </div>
                        <div className="firstRowPanouAngajat">
                            <div className="firstRowPanouAngajatFirstColumn">
                                <label className="firstRowPanouAngajatFirstColumnLabel">Telefon: </label>
                            </div>
                            <div className="firstRowPanouAngajatSecondColumn">
                                <textarea className="firstRowPanouAngajatTextBoxDenumire" name="denumire" rows="1" 
                                            onChange={(e)=>{setTelefon(e.target.value)}}>
                                </textarea>
                            </div>
                        </div>
                        <div className="firstRowPanouAngajat">
                            <div className="firstRowPanouAngajatFirstColumn">
                                <label className="firstRowPanouAngajatFirstColumnLabel">Email: </label>
                            </div>
                            <div className="firstRowPanouAngajatSecondColumn">
                                <textarea className="firstRowPanouAngajatTextBoxDenumire" name="denumire" rows="1" 
                                            onChange={(e)=>{setEmail(e.target.value)}}>
                                </textarea>
                            </div>
                        </div>
                        <div className="firstRowPanouAngajat">
                            <div className="firstRowPanouAngajatFirstColumn">
                                <label className="firstRowPanouAngajatFirstColumnLabel">Parola: </label>
                            </div>
                            <div className="firstRowPanouAngajatSecondColumn">
                                <input type='password' className="firstRowPanouAngajatTextBoxDenumire" name="denumire" 
                                            onChange={(e)=>{setParola(e.target.value)}}>
                                </input>
                            </div>
                        </div>
                        <div className="firstRowPanouAngajat">
                            <div className="firstRowPanouAngajatFirstColumn">
                                <label className="firstRowPanouAngajatFirstColumnLabel">Descriere: </label>
                            </div>
                            <div className="firstRowPanouAngajatSecondColumn">
                                <textarea className="firstRowPanouAngajatTextBoxDescriere" name="denumire" rows="1" 
                                            onChange={(e)=>{setDescriere(e.target.value)}}>
                                </textarea>
                            </div>
                        </div>
                        
                        <div className="secondRowPanouAngajatAdd">
                            <button className="firstRowPanouAngajatInsertButton" onClick={()=>{adaugareAngajat()}}>Adauga angajat nou</button>
                        </div>
                </div>
            </div>
            )}
            {!isLogged && (
                <PanouAutentificare/>
            )}
        </>
        
    )
}
export default PanouAdaugareAngajat;