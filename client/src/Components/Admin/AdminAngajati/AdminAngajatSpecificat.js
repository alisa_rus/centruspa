import React,{useState,useEffect} from 'react';
import Axios from 'axios';
import { useParams } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import NavBar from '../../NavBarAdmin/NavBar';
import PanouAngajat from './PanouAngajat';
import PanouAutentificare from '../../PanouAutentificare/PanouAutentificare';

function AdminAngajatSpecificat(){

    let { id } = useParams();
    let navigate=useNavigate();

    const [content,setContent]=useState([]);
    const [isLogged,setLogged]=useState(false);

    useEffect(()=>{

        const checkIfLogin=()=>{
            if(window.localStorage.getItem("token") && window.localStorage.getItem("role")==='1')
                {
                    setLogged(true);
                    Axios.get(`http://localhost:3001/user/angajatById/${id}`).then((response) => {
                        setContent(response.data);
                    });
                }    
            else 
            {
                setLogged(false);
                navigate('/autentificare');
            }
        }
        checkIfLogin();    
    
    },[id,navigate]) 

    return(
        <>  
            {isLogged && (
            <div className="adminAngajatiPanou">
                <div className="adminAngajatiNavBar">
                    <NavBar/>
                </div>
                <div className="adminAngajatSpecificatColumn">
                    <p className="panouAngajatP"> Vizualizeaza detaliile contului</p>  
                    {content.map((val)=>{
                        return(
                        <PanouAngajat key={val.Cod_utilizator} Cod_utilizator={val.Cod_utilizator} Nume={val.Nume} Prenume={val.Prenume}
                        Email={val.Email} Parola={val.Parola} Telefon={val.Telefon} Descriere={val.Descriere} Imagine={val.Imagine}
                        />)}       
                    )}
                </div>           
            </div>
            )}
            {!isLogged && (
                <PanouAutentificare/>
            )}
            
        </>
    )
}
export default AdminAngajatSpecificat;