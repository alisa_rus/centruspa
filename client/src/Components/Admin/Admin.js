import React from 'react'
import './Admin.css'
import NavBar  from '../NavBarAdmin/NavBar';

function Admin(){
    
    return(
        <>
            <NavBar/>
            <h1 className="h1AdminPage">Panoul de administrare a aplicatiei</h1>
        </>
    )
}
export default Admin;