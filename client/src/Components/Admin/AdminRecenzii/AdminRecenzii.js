import React,{useState,useEffect} from 'react';
import { useNavigate } from 'react-router-dom';
import Axios from 'axios';
import NavBar  from '../../NavBarAdmin/NavBar';
import './AdminRecenzii.css';
import PanouAutentificare from '../../PanouAutentificare/PanouAutentificare';

function AdminRecenzii(){

    let navigate=useNavigate();

    const [content,setContent]=useState([]);
    const [isLogged,setLogged]=useState(false);

    const getRecenzii=()=>{
        Axios.get("http://localhost:3001/recenzii").then((response) => {
        setContent(response.data);
        });
    }
    const stergeRecenzia=(Cod_recenzie)=>{
        Axios.delete(`http://localhost:3001/recenzii/stergereRecenzie/${Cod_recenzie}`).then((response)=>{
            alert(response.data);
            if(response.data==='Recenzie stearsa cu succes.')
                    window.location.reload(true);

        });
    }
    useEffect(()=>{

        const checkIfLogin=()=>{
            if(window.localStorage.getItem("token") && window.localStorage.getItem("role")==='1')
                {
                    setLogged(true);     
                    getRecenzii();
                }
            else 
            {
                setLogged(false);
                navigate('/autentificare');
            }
        }
        checkIfLogin();

    },[navigate]);

    return(
        <>
            {isLogged && (
            <div className="adminRecenziiPanou">
                <div className="adminRecenziiNavBar">
                    <NavBar/>
                </div>
                <div className="adminRecenzii">
                    <div className="adminRecenziiRow">
                        <p className="pAdminRecenziiRow">RECENZII EXISTENTE</p>
                        <div className="adminRecenziiRow2">
                            <table className="tabelRecenzii">
                                <thead>
                                    <tr className="randTabelRecenzii">
                                        <th className="hTabelRecenzii">Angajat</th>
                                        <th className="hTabelRecenzii">Serviciu</th>
                                        <th className="hTabelRecenzii">Nota</th>
                                        <th className="hTabelRecenzii">Mesaj</th>
                                        <th className="hTabelRecenzii">Sterge recenzia</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {content.map((val)=>{
                                    return(
                                        <tr className="randDateTabelRecenzii" key={val.Cod_recenzie}>
                                            <td><div  className="randDateTabelRecenziiMesaj">{val.Nume} {val.Prenume}</div></td>
                                            <td><div  className="randDateTabelRecenziiMesaj">{val.Denumire}</div></td>
                                            <td><div  className="randDateTabelRecenziiMesaj">{val.Nota}</div></td>
                                            <td><div  className="randDateTabelRecenziiMesajMesaj">{val.Mesaj}</div></td>
                                            <td><button className="tdButonRecenzii" onClick={()=>{stergeRecenzia(val.Cod_recenzie)}}>STERGE</button></td>
                                        </tr>
                                    )
                                    })}
                                </tbody>
                            
                            </table>
                        </div>                   
                    </div>
                </div>
            </div>          
            )}
            {!isLogged && (
                <PanouAutentificare/>
            )}
            
        </>
    )
}
export default AdminRecenzii;