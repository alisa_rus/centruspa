import React, { useState, useEffect} from 'react';
import Axios from 'axios';
import CardCategorie from './CardCategorie';
import NavBar from '../NavBar/NavBar';
import Footer from '../Footer/Footer';
import './CardCategorie.css'
import './../NavBar/NavBar.css'

function Categorie(){
    
    const [categorii,setCategorii]=useState([]);

    useEffect(()=>{
         Axios.get("http://localhost:3001/categorii").then((response) => {
         setCategorii(response.data);
         });
    },[]);

    return(
    <div  className='divCategorie'>
        <NavBar/>
        {categorii.map((val)=>{
            return(
                    <CardCategorie key={val.Cod_categorie} ID_categorie={val.Cod_categorie}
                     Descriere={val.Descriere} Denumire={val.Denumire} Imagine={val.Imagine}/>
            )
        })}
       <Footer/>
    </div>)

}

export default Categorie;