import React from 'react';
import { useNavigate } from 'react-router-dom';
import './CardCategorie.css';

function CardCategorie(props){
    
    let ID_categorie=props.ID_categorie;
    let navigate= useNavigate();
    const redirect = () => {
    navigate(`/servicii/${ID_categorie}`);
    }

    return(
     
            <div className="card_bodyCat">
                <p className="card_title">{props.Denumire}</p>
                <img className="card_image" src={`images/categorii/${props.Imagine}`} alt="error"/>
                <p className="card_description">{props.Descriere}</p>
                <button className="card_button" onClick={redirect}>Vezi toate serviciile</button>
            </div>
        
    )
}
export default CardCategorie;
