import  Axios  from 'axios';
import React,{useState,useEffect} from 'react';
import {AiFillCloseCircle} from "react-icons/ai";
import ProgramareNoua from './ProgramareNoua';


function ProgramariClient(props){

    const [id]=useState(props.Cod_client);
    const [programari,setProgramari]=useState([]);
    const [programareNoua,setProgramareNoua]=useState(false);

    useEffect(()=>{
        Axios.get(`http://localhost:3001/user/clienti/programariClienti/${id}`).then((response)=>{
            setProgramari(response.data);
        })
    },[id]);

    const stergereProgramare=(id)=>{
        Axios.delete(`http://localhost:3001/programari/stergereProgramare/${id}`).then((response)=>{
            alert(response.data);
            if(response.data==='Programare stearsa cu succes.')
                window.location.reload(true);
        });  
    }

    return(
        <>  
            <div className='panouProgramariMainDiv panouProgramariClientMainDiv'>
                {!programareNoua && (
                    <>
                        <div className='programariInsert'>
                            <button className='butonInsertProgramare' onClick={()=>{setProgramareNoua(true)}}>PROGRAMARE NOUA</button>
                        </div>
                        <table className="tabelProgramariAngajat tabelProgramariClient">
                            <thead>
                                <tr className="randTabelProgramariAngajat">
                                    <th className="hTabelProgramariAngajat">Serviciu</th>
                                    <th className="hTabelProgramariAngajat">Status</th>
                                    <th className="hTabelProgramariAngajat">Data</th>
                                    <th className="hTabelProgramariAngajat">Ora</th>
                                    <th className="hTabelProgramariAngajat">Pret</th>
                                    <th className="hTabelProgramariAngajat">Anuleaza programare</th>
                                </tr>
                            </thead>
                            <tbody>                                  
                                {programari.map((val)=>{     
                                    let x=val.OraStart;     
                                return(                   
                                        <tr className="randDateTabelProgramariAngajat" key={val.Cod_programare}>
                                            <td>{val.Denumire}</td>
                                            <td>{val.Status}</td>
                                            <td>{val.Zi}</td>
                                            <td>{x.substr(0,5)}</td>
                                            <td>{val.Pret} lei</td>
                                            <td>
                                                <div className=" close closeX">
                                                    <AiFillCloseCircle size={20} className="iconClose" onClick={()=>{stergereProgramare(val.Cod_programare)}}/>
                                                </div>
                                            </td>
                                        </tr>       
                                )
                                })}
                            </tbody>
                        </table>             
                    </>
                   
                )}
                {programareNoua && (
                    <ProgramareNoua/>
                )}
                
            </div>                        
        </>
    )
}
export default ProgramariClient;