import React,{useState} from 'react';
import Axios from 'axios';
import './PanouDedicatClient.css';

function DetaliiCont(props){

    const [nume, setNume]=useState(props.Nume);
    const [prenume, setPrenume]=useState(props.Prenume);
    const [Cod_utilizator]=useState(props.Cod_client);
    const [email, setEmail]=useState(props.Email);
    const [parola, setParola]=useState(props.Parola);
    const [telefon,setTelefon]=useState(props.Telefon);
    const [flagParola,setFlagParola]=useState(false);

    const modificaContClient=()=>{

        if(nume===''||prenume===''||telefon===''||email===''||parola==='')
            alert("Completati toate campurile.");
        else
        {
            Axios.put("http://localhost:3001/user/clienti/modificareClient",{
                Cod_utilizator:Cod_utilizator,
                Nume:nume,
                Prenume:prenume,
                Telefon:telefon,
                Email:email,
                Parola:parola,
                Flag_parola:flagParola,
            }
            ).then((response)=>{
                alert(response.data);
                if(response.data==='Cont client modificat cu succes.')
                    window.location.reload(true);
            })
        }
    }

    return(
        <div className="panouDetaliiCont">  
            <div className="firstRowPanouClient">
                <div className="firstRowPanouClientFirstColumn">
                    <label className="firstRowPanouClientFirstColumnLabel">Nume: </label>
                </div>
                <div className="firstRowPanouClientSecondColumn">
                    <textarea className="firstRowPanouClientTextBoxDenumire" name="denumire" rows="1" 
                            defaultValue={props.Nume} onChange={(e)=>{setNume(e.target.value)}}>
                    </textarea>
                </div>
            </div>
            <div className="firstRowPanouClient">
                <div className="firstRowPanouClientFirstColumn">
                    <label className="firstRowPanouClientFirstColumnLabel">Prenume: </label>
                </div>
                <div className="firstRowPanouClientSecondColumn">
                    <textarea className="firstRowPanouClientTextBoxDenumire" name="denumire" rows="1" 
                            defaultValue={props.Prenume} onChange={(e)=>{setPrenume(e.target.value)}}>
                    </textarea>
                </div>
            </div>
            <div className="firstRowPanouClient">
                <div className="firstRowPanouClientFirstColumn">
                    <label className="firstRowPanouClientFirstColumnLabel">Telefon: </label>
                </div>
                <div className="firstRowPanouClientSecondColumn">
                    <textarea className="firstRowPanouClientTextBoxDenumire" name="denumire" rows="1" 
                            defaultValue={props.Telefon} onChange={(e)=>{setTelefon(e.target.value)}}>
                    </textarea>
                </div>
            </div>
            <div className="firstRowPanouClient">
                <div className="firstRowPanouClientFirstColumn">
                    <label className="firstRowPanouClientFirstColumnLabel">Email: </label>
                </div>
                <div className="firstRowPanouClientSecondColumn">
                    <textarea className="firstRowPanouClientTextBoxDenumire" name="denumire" rows="1" 
                            defaultValue={props.Email} onChange={(e)=>{setEmail(e.target.value)}}>
                    </textarea>
                </div>
            </div>
            <div className="firstRowPanouClient">
                <div className="firstRowPanouClientFirstColumn">
                    <label className="firstRowPanouClientFirstColumnLabel">Parola: </label>
                </div>
                <div className="firstRowPanouClientSecondColumn">
                    <input type='password' className="firstRowPanouClientTextBoxDenumire" name="denumire" rows="1" 
                            defaultValue={props.Parola} onChange={(e)=>{setParola(e.target.value);setFlagParola(true)}}>
                    </input>
                </div>
            </div>
            <div className="secondRowPanouClientDetaliiCont">
                    <button className="firstRowPanouClientiSubmitButtonDetaliiCont" onClick={modificaContClient}>Editeaza detalii cont</button>
            </div>
        </div>

    )
}
export default DetaliiCont;