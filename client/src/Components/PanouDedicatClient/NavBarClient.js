import React from 'react'
import './NavBarClient.css'
import { NavLink } from 'react-router-dom';
import {useNavigate } from 'react-router-dom';

function NavBarClient(){

    let navigate=useNavigate();

    const logout = async (e) => {
        navigate('/');
        window.localStorage.clear();
       
    };
    
    return(
        <div className="navBar_body">
            <div className="navBar_p navBar_img">
            <img src="/logo2.png" alt="error"className="navBar_img" />
            </div>
            <NavLink to="/" className="navBar_p">Acasa</NavLink>
            <NavLink  to="/despreNoi" className="navBar_p">Despre noi</NavLink>
            <NavLink  to="/categorii" className="navBar_p">Serviciile noastre</NavLink>
            <NavLink  to="/echipa" className="navBar_p">Echipa noastra</NavLink>
            <button className="navBarClientiButton" onClick={()=>{logout()}}>Deconectare</button>
        </div>
    )
    
}
export default NavBarClient;