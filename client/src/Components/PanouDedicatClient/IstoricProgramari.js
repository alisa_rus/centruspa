import  Axios  from 'axios';
import React,{useState,useEffect} from 'react';

function IstoricProgramari(props){

    const [id]=useState(props.Cod_client);
    const [programari,setProgramari]=useState([]);

    useEffect(()=>{
        Axios.get(`http://localhost:3001/user/clienti/programariFinalizateClienti/${id}`).then((response)=>{
            setProgramari(response.data);
            console.log(response.data);
        })
    },[id]);

    return(
        <>
            <div className='panouProgramariMainDiv panouProgramariClientMainDiv panouIstoricProgramariClientMainDiv'>
                <table className="tabelProgramariAngajat tabelProgramariIstoricClient">
                    <thead>
                        <tr className="randTabelProgramariAngajat">
                            <th className="hTabelProgramariAngajat">Serviciu</th>
                            <th className="hTabelProgramariAngajat">Status</th>
                            <th className="hTabelProgramariAngajat">Data</th>
                            <th className="hTabelProgramariAngajat">Ora</th>
                            <th className="hTabelProgramariAngajat">Pret</th>
                        </tr>
                    </thead>
                    <tbody>                                  
                        {programari.map((val)=>{     
                            let x=val.OraStart;     
                        return(                   
                                <tr className="randDateTabelProgramariAngajat" key={val.Cod_programare}>
                                    <td>{val.Denumire}</td>
                                    <td>{val.Status}</td>
                                    <td>{val.Zi}</td>
                                    <td>{x.substr(0,5)}</td>
                                    <td>{val.Pret} lei</td>
                                </tr>       
                        )
                        })}
                    </tbody>
                </table>             
            </div>                        
        </>
    )
}
export default IstoricProgramari;