import React,{useState,useEffect} from 'react';
import Axios from 'axios';
import './RecenzieNoua.css';

function RecenzieNoua(){

    

    const [servicii,setServicii]=useState([]);
    const [angajati,setAngajati]=useState([]);
    const [note]=useState([1,2,3,4,5,6,7,8,9,10]);

    const [viewServicii,setViewServicii]=useState(false);
    const [viewAngajati,setViewAngajati]=useState(false);
    const [viewNota,setViewNota]=useState(false);
    const [viewMesaj,setViewMesaj]=useState(false);
    const [viewButton,setViewButton]=useState(false);

    const [CodServiciu,setCodServiciu]=useState();
    const [CodAngajat,setCodAngajat]=useState();
    const [nota,setNota]=useState();
    const [mesaj,setMesaj]=useState();

    function getServicii(){
        Axios.get(`http://localhost:3001/servicii/`).then((response) => {
         setServicii(response.data);
         setViewServicii(true);
         });
    }
    
    function getAngajati(id){
        Axios.get(`http://localhost:3001/user/angajatByServiceId/${id}`).then((response)=>{
            setCodAngajat(response.data[0].Cod_utilizator);
            setAngajati(response.data);
            setViewAngajati(true);
        })
    }

    function adaugaRecenzie(){
        Axios.post("http://localhost:3001/recenzii/adaugaRecenzie",{      
            Cod_angajat:CodAngajat,
            Cod_serviciu:CodServiciu,
            Nota:nota,
            Mesaj:mesaj
        }).then((response)=>{
            alert(response.data);
            if(response.data==='Recenzie adaugata cu succes.')
                window.location.reload();
        })
    }
    useEffect(()=>{
        getServicii();
    })

    return(
        <div className='divRecenzieNoua'>
            <p className='pRecenzieNoua'>Alege serviciul pentru care doresti sa adaugi o recenzie</p>
            {viewServicii && (
                <select className="selectServicii selectServiciiRecenzieNoua"  onChange={(event)=>{setViewAngajati(false);setViewMesaj(false);setViewNota(false);setViewButton(false);setCodServiciu(event.target.value);getAngajati(event.target.value);}}>
                    <option ></option>
                    {servicii.map((option) => (
                        <option key={option.Cod_serviciu} value={option.Cod_serviciu}>{option.Denumire}</option>
                    ))}
                </select>
                )
            }   
            {viewAngajati && (
                <>
                    <p className='pRecenzieNoua'>Alege angajatul caruia doresti sa ii adaugi o recenzie</p>
                    <select className="selectServicii selectServiciiRecenzieNoua"  onChange={(event)=>{setCodAngajat(event.target.value);setViewNota(true);setViewMesaj(false);setViewButton(false)}}>
                        <option></option>
                        {angajati.map((option) => (
                            <option key={option.Cod_utilizator} value={option.Cod_utilizator}>{option.Nume} {option.Prenume}</option>
                        ))}
                    </select>
                </>    
                )
            }
            {viewNota && (
                <>
                     <p className='pRecenzieNoua'>Alege o nota</p>
                    <select className="selectServicii selectServiciiRecenzieNoua"  onChange={(event)=>{setNota(event.target.value);setViewMesaj(true)}}>
                        <option></option>
                        {note.map((option) => (
                            <option key={option} value={option}>{option}</option>
                        ))}
                    </select>
                </>
            )}
            {
                viewMesaj && (
                    <>
                        <p className='pRecenzieNoua'>Adauga un mesaj</p>
                        <input type='text' className='mesajRecenzie' onChange={(e)=>{setMesaj(e.target.value);setViewButton(true)}}></input>
                    </>
                )
            }
            {viewButton && (
                <button onClick={adaugaRecenzie} className="butonFinalizeazaRecenzie">ADAUGA RECENZIA</button>    
            )}
        </div>
    )
}
export default RecenzieNoua;