import React,{useState,useEffect} from 'react';
import { useParams} from 'react-router-dom';
import Axios from 'axios';
import DatePicker from 'react-datetime';
import moment from 'moment';
import 'react-datetime/css/react-datetime.css';
import './ProgramareNoua.css';

function ProgramareNoua(){

    let {id}=useParams();

    const [servicii,setServicii]=useState([]);
    const [angajati,setAngajati]=useState([]);

    const [viewServicii,setViewServicii]=useState(false);
    const [viewAngajati,setViewAngajati]=useState(false);
    const [viewDuration,setViewDuration]=useState(false);
    const [viewCalendar,setViewCalendar]=useState(false);
    const [viewButton,setViewButton]=useState(false);

    const [CodServiciu,setCodServiciu]=useState();
    const [CodAngajat,setCodAngajat]=useState();
    const [CodClient]=useState(id);
    const [Zi,setZi]=useState();
    const [OraStart,setOraStart]=useState();
    const [Pret,setPret]=useState();

    const [duration,setDuration]=useState(0);
    const [time,setTime]=useState([]);
    const [filteredTime,setFilteredTime]=useState([]);

    const yesterday = moment().subtract(0, 'day');
    const disablePastDt = current => {
        return current.isAfter(yesterday)&&current.day() !== 0 && current.day() !== 6;
    };

    function getServicii(){
        Axios.get(`http://localhost:3001/servicii/`).then((response) => {
         setServicii(response.data);
         setViewServicii(true);
         });
    }
    
    function getAngajati(id){
        Axios.get(`http://localhost:3001/user/angajatByServiceId/${id}`).then((response)=>{
            setCodAngajat(response.data[0].Cod_utilizator);
            setAngajati(response.data);
            setViewAngajati(true);
        })
    }

    function getDuration(id){
        Axios.get(`http://localhost:3001/servicii/${id}`).then((response) => {
            setDuration(response.data[0].Durata);
        });  
    }

    function checkTime(zi,OraStart){ 
        while(filteredTime.length>0)
            filteredTime.pop();
        Axios.post(`http://localhost:3001/programari/verificareProgramare`,{
            Cod_angajat:CodAngajat,
            Cod_serviciu:CodServiciu,
            Zi:zi,
        }).then((response) => {
            setViewDuration(true);
            response.data.forEach((val)=>{ val=val.OraStart.substr(0,5);filteredTime.push(val);})
        })   
    }

    function getTime(duration,zi){
        console.log(filteredTime);
        let time=[];
        let hour=10;
        let minutes=0;

        while(hour<18)
        {   
            if(minutes===0) time.push(hour+":"+minutes+'0')
            else time.push(hour+":"+minutes)
            if(minutes+duration<60)
                minutes=minutes+duration;
            else if((minutes+duration)===60)
                {
                    hour=hour+1;
                    minutes=0;
                }
            else if(minutes+duration>60)
                {
                    hour=hour+Math.floor((minutes+duration)/60);
                    minutes=duration-(60*Math.floor((minutes+duration)/60)-minutes);
                }
        }
        
        setTime(time);
        checkTime(zi,hour);  
    }

    function showTime(){
        
        setTime(time.filter( function( el ) {
            return filteredTime.indexOf( el ) < 0;
          }))
    }

    function setPretProgramare(id){
        Axios.get(`http://localhost:3001/servicii/${id}`).then((response) => {
            setPret(response.data[0].Pret);
        }); 
    }

    function adaugaProgramare(){
        Axios.post("http://localhost:3001/programari/adaugaProgramare",{
            Cod_client:CodClient,
            Cod_angajat:CodAngajat,
            Cod_serviciu:CodServiciu,
            Status:"NECONFIRMATA",
            Zi:Zi,
            OraStart:OraStart,
            Pret:Pret
        }).then((response)=>{
            alert(response.data);
            if(response.data==='Programare finalizata cu succes.')
                window.location.reload();
        })
    }
    useEffect(()=>{
        getServicii();
    })

    return(
        <div className='divProgramareNoua'>
            <div className='programariInsert'>
                <button className='butonInsertProgramare' onClick={()=>{window.location.reload(true)}}>RENUNTA LA FORMULAR</button>
            </div>
            <p className='pProgramareNoua'>Alege serviciul de care vrei sa beneficiezi</p>
            {viewServicii && (
                <select className="selectServicii selectServiciiProgramareNoua"  onChange={(event)=>{setViewAngajati(false);setViewDuration(false);setViewCalendar(false);setPretProgramare(event.target.value);setCodServiciu(event.target.value);setFilteredTime([]);getAngajati(event.target.value);getDuration(event.target.value)}}>
                    <option ></option>
                    {servicii.map((option) => (
                        <option key={option.Cod_serviciu} value={option.Cod_serviciu}>{option.Denumire}</option>
                    ))}
                </select>
                )
            }   
            {viewAngajati && (
                <>
                    <p className='pProgramareNoua'>Alege un membru al echipei noastre care sa iti ghideze <i> calatoria</i> in centrul nostru SPA</p>
                    <select className="selectServicii selectServiciiProgramareNoua"  onChange={(event)=>{setCodAngajat(event.target.value);setViewDuration(false);setViewCalendar(true);setFilteredTime([])}}>
                        <option></option>
                        {angajati.map((option) => (
                            <option key={option.Cod_utilizator} value={option.Cod_utilizator}>{option.Nume} {option.Prenume}</option>
                        ))}
                    </select>
                </>    
                )
            }
            {viewCalendar && (
                <>
                    <p className='pProgramareNoua'>Alege ziua in care vrei sa beneficiezi de serviciul nostru</p>
                    <DatePicker  dateFormat='YYYY-MM-DD' isValidDate={disablePastDt}  onChange={(date) => {getTime(duration,date.format().substr(0,10));setZi(date.format().substr(0,10))}}/>
                </>
                
            )}
            {viewDuration && (
                <>
                    <p className='pProgramareNouaDuration'>Alege ora la care vrei sa beneficiezi de serviciul nostru</p>
                    <select className="selectServicii selectServiciiProgramareNoua"  onClick={showTime} onChange={(event)=>{setOraStart(event.target.value);setViewButton(true)}}>
                        <option ></option>
                        {time.map((option) => (
                            <option key={option} value={option}>{option}</option>
                        ))}
                    </select>
                </>    
            )
            }
            {viewButton && (
                <button onClick={adaugaProgramare} className="butonFinalizeazaProgramare">FINALIZEAZA PROGRAMARE</button>    
            )}
        </div>
    )
}
export default ProgramareNoua;