import React,{ useState,useEffect} from 'react';
import  Axios from 'axios';
import { useParams,useNavigate } from 'react-router-dom';
import {BiUser} from 'react-icons/bi';
import {MdOutlineSchedule} from 'react-icons/md';
import {AiOutlineSchedule} from 'react-icons/ai';
import {MdOutlineGrade} from 'react-icons/md';
import NavBarClient from './NavBarClient';
import DetaliiCont from './DetaliiCont';
import ProgramariClient from './ProgramariClient';
import IstoricProgramari from './IstoricProgramari';
import './PanouDedicatClient.css';
import './MeniuClient.css';
import RecenzieNoua from './RecenzieNoua';

function PanouDedicatClient(){

    let {id}=useParams();
    let navigate=useNavigate();

    const [showDetaliiCont,setShowDetaliiCont]=useState(false);
    const [showProgramari,setShowProgramari]=useState(false);
    const [showIstoric,setShowIstoric]=useState(false);
    const [showRecenzii,setShowRecenzii]=useState(false);

    const [isLogged,setLogged]=useState(false);
    const [detalii,setDetalii]=useState([]);

    const getDetaliiCont=()=>{
        Axios.get(`http://localhost:3001/user/clientById/${id}`).then((response) => {
            setDetalii(response.data);
            setShowRecenzii(false);
            setShowProgramari(false);
            setShowIstoric(false);
            setShowDetaliiCont(true);
        });
    }

    useEffect(()=>{

        const checkIfLogin=()=>{
            if(window.localStorage.getItem("token") && window.localStorage.getItem("role")==='3')
                {
                    setLogged(true);
                }
            else 
            {
                setLogged(false);
                navigate('/autentificare');
            }
        }
        checkIfLogin();
    },[navigate]);


    return(
        <>
            {isLogged && (
                <div className='panouDedicatClientMainDiv'>
                    <NavBarClient/>
                    <div className='panouDedicatClientFirstRow'>
                        <div className='MeniuClient'>
                            <div className='meniuClientContent'>
                                <p className='pMeniuClientContent' onClick={()=>{getDetaliiCont()}}><BiUser  style={{marginBottom:"-2px"}}/> Contul meu</p>
                                <p className='pMeniuClientContent' onClick={()=>{setShowDetaliiCont(false); setShowIstoric(false);setShowProgramari(true);setShowRecenzii(false)}}><AiOutlineSchedule style={{marginBottom:"-2px"}}/>  Programarile mele</p>
                                <p className='pMeniuClientContent' onClick={()=>{setShowDetaliiCont(false); setShowIstoric(true);setShowProgramari(false);setShowRecenzii(false)}}><MdOutlineSchedule style={{marginBottom:"-2px"}}/> Istoric programari</p>
                                <p className='pMeniuClientContent' onClick={()=>{setShowDetaliiCont(false); setShowIstoric(false);setShowProgramari(false);setShowRecenzii(true)}}><MdOutlineGrade style={{marginBottom:"-2px"}}/> Adauga recenzie</p>
                                <img className='imgMeniuClient' src='/logoaplicatie.png' alt='error'></img>
                            </div>
                        </div>
                        <div className='panouDedicatClientFirstRowInfo'>
                        { showDetaliiCont && (
                                detalii.map((val)=>{
                                    return(
                                        <DetaliiCont Cod_client={val.Cod_utilizator} Nume={val.Nume} Prenume={val.Prenume} Telefon ={val.Telefon} Email={val.Email} Parola={val.Parola} key={val.Cod_utilizator}/>
                                    )
                                
                                })     
                            )
                        }
                        {
                            showProgramari && (
                                <ProgramariClient Cod_client={id}/>
                            )
                        }
                        {
                            !showDetaliiCont && !showProgramari && !showIstoric && !showRecenzii &&(
                                <div className='detaliiContWelcome'>
                                    Bine ai venit!<br></br> Ne bucuram ca ai ales serviciile complexului nostru! Programeaza-te din timp pentru o experienta de neuitat! 
                                    <img src='/logo3.png' alt='error' className='detaliiContWelcomeImg'></img>
                                </div>
                            )
                        }
                        {
                            showIstoric && (
                                <IstoricProgramari Cod_client={id}/>
                            )
                        }
                        {
                            showRecenzii && (
                                <div>
                                    <RecenzieNoua/>
                                </div>
                            )
                        }
                        </div>       
                    </div>
                </div>
            )}  
        </>      
    )
}
export default PanouDedicatClient;