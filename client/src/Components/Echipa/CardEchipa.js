import React, { useEffect, useState } from "react";
import Axios from 'axios';
import './CardEchipa.css';
import './PopUp.css';
import { AiTwotoneMail } from 'react-icons/ai';
import {BsFillTelephoneFill} from 'react-icons/bs';
import {BsFillInfoCircleFill} from 'react-icons/bs';
import {AiOutlineClose} from 'react-icons/ai';
import {FaSpa} from 'react-icons/fa';
import {MdGrade} from 'react-icons/md';

function CardEchipa(props){

    const [modal, setModal] = useState(false);
    const [servicii, setServicii]=useState([]);
    let note=[];
    const [nota,setNota]=useState([]);
    const [nrRecenzii,setNrRecenzii]=useState([]);
    let id=props.id;

    const getServiciiAngajat=(id)=>{
        Axios.get(`http://localhost:3001/user/echipa/serviciiAngajat/${id}`).then((response) => {
            setServicii(response.data);
        });
    }

    const getRecenzii=(id)=>{
        Axios.get(`http://localhost:3001/user/echipa/recenziiAngajat/${id}`).then((response) => {
            response.data.forEach(element => {
                note.push(element.Nota);
            });
        });   
    }

    const sum=(note)=>{
        let suma=0;
        let cnt=0;
        note.forEach((element)=>{
          suma=suma+element;
          cnt++;
        })
        setModal(!modal);
        if(cnt===0) setNota(0);
        else setNota(suma/cnt);
        setNrRecenzii(cnt);
    }
    
    const toggleModal = () => {
        sum(note);
        getServiciiAngajat(id); 
    };

    if(modal) {
        document.body.classList.add('active-modal')
    } else {
        document.body.classList.remove('active-modal')
    }

    useEffect(()=>{
        getRecenzii(id);
    })

    return(
            <>
            <div className="card_body card_body_echipa">
                <img className=" card_image card_image_echipa" src={`images/echipa/${props.Imagine}`}   alt="error"/>
                <p className="card_title">{props.Nume} {props.Prenume}</p>
                <p className="card_notes"><AiTwotoneMail/>  Adresa de e-mail</p>
                <p className="card_notes_info">{props.Email}</p>
                <p className="card_notes"><BsFillTelephoneFill/>  Numar de telefon</p>
                <p className="card_notes_info">{props.Telefon}</p>
                <p className="card_notes"><BsFillInfoCircleFill onClick={toggleModal} style={{cursor:'pointer'}} className='info'/>  Despre {props.Prenume}</p>
                <p className="card_notes_description">{props.Descriere}</p>
                
            </div>
            {modal && (
                <div className="modal">
                    <div onClick={toggleModal} className="overlay"></div>
                    <div className="modal-content">
                    <h4><FaSpa style={{marginBottom:'-2px'}}/> Serviciile prestate de {props.Prenume} sunt: </h4>
                    <ul>
                    {servicii.map((val)=>{
                        return(<li key={val.Cod_serviciu}>
                            {val.Denumire}
                            </li>
                        )
                        })
                    }
                    </ul>
                    <h4><MdGrade style={{marginBottom:'-2px'}}/> {props.Prenume} a obtinut nota: {nota}/10 din {nrRecenzii} recenzii </h4>
                    <h4><BsFillTelephoneFill style={{marginBottom:'-2px'}}/> Apeleaza cu incredere pentru rezultate imediate! </h4>
                    <button className="close-modal" onClick={toggleModal}>     
                        <AiOutlineClose/>
                    </button>
                    </div>
                </div>
            )}
            </>
        
    )
}
export default CardEchipa;
