import React, { useState, useEffect} from 'react';
import Axios from 'axios';
import NavBar from '../NavBar/NavBar';
import Footer from '../Footer/Footer';
import CardEchipa from './CardEchipa';
import './../NavBar/NavBar.css'

function Echipa(){
    
    const [echipa,setEchipa]=useState([]);

    useEffect(()=>{
         Axios.get("http://localhost:3001/user/echipa").then((response) => {
         setEchipa(response.data);
         });
    },[]);

    return(
    <div className='echipa'>
        <NavBar/>
        <p></p>
        <div id="popUpPlace"/>
        {echipa.map((val)=>{
            return(
                    <CardEchipa key={val.Cod_utilizator} id={val.Cod_angajat} Nume={val.Nume} Prenume={val.Prenume}
                     Telefon={val.Telefon} Email={val.Email} Imagine={val.Imagine} Descriere={val.Descriere}/>
            )
        })}
        <Footer/>
    </div>)

}

export default Echipa;