import styled from "styled-components";

export default styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 65vh;
  width: 100vw;
  background-color:faebd75c;
  color: #fff;
  margin: 0 15px;
  font-size: 4em;
`;
