import React from "react";
import NavBar from "../NavBar/NavBar";
import Footer from "../Footer/Footer";
import Carousel from "react-elastic-carousel";
import Item from "./Item";
import "./Carusel.css";
import { useNavigate } from 'react-router-dom';

const breakPoints = [
    { width: 500, itemsToShow: 1 },
  ];


function Acasa(){

    let navigate=useNavigate();

    const redirect = () => {
    
        navigate(`/categorii`);
    }
    
    const redirectToEchipa = () => {
        
        navigate(`/echipa`);
    }

    return(
        <>
        <NavBar/>  
        <div className="Carusel">
            <Carousel breakPoints={breakPoints}>
                    <Item>
                        <img className="imgCarusel" src="images/acasa/carusel3.jpeg" alt="error">
                        </img>
                    </Item>
                    <Item>
                        <img className="imgCarusel" src="images/acasa/carusel2.jpg" alt="error">
                        </img>
                    </Item>
                    <Item>
                        <img className="imgCarusel" src="images/acasa/carusel1.jpg" alt="error">
                        </img>
                    </Item>          
            </Carousel>
        </div>
        <h1 className="textS" onClick={redirect}>Corpul tau merita o doza de relaxare. Lasa-te surprins de serviciile noastre!</h1>
        <div className="row">
            <div className="column">
                <img className="imgAcasa" src="images/acasa/acasa1.jpg" alt="error"></img>
                <p className="pAcasa">Bucura-te de un moment sanatos de relaxare visand la o calatorie de vacanta.</p>
            </div>
            <div className="column">
                <img className="imgAcasa" src="images/acasa/acasa2.jpg" alt="error"></img>
                <p className="pAcasa">Pune sănătatea pe primul loc și răsfață-te cu o zi minunata la spa în arome tropicale.</p>
            </div>
            <div className="column">
                <img className="imgAcasa" src="images/acasa/acasa3.jpg" alt="error"></img>
                <p className="pAcasa" >Imbunătățește-ți sistemul imunitar pen-tru sezonul rece cu un tratament spa grozav.</p>
            </div>
            <div className="column">
                <img className="imgAcasa" src="images/acasa/acasa4.jpg" alt="error"></img>
                <p className="pAcasa" >Vindecarea este primul pas către ferici-rea ta. Inlocuieste rutina si vino o zi la spa.</p>
            </div>
        </div>
        <h1 className="textP" onClick={redirectToEchipa}>Ai incredere in echipa noastra de profesionisti!</h1>
        <Footer/>
        </>
    )

}
export default Acasa;