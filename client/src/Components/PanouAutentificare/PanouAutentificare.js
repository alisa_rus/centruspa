import React, {useState,useEffect } from "react";
import {useNavigate } from 'react-router-dom';
import Axios from "axios";
import NavBar from '../NavBar/NavBar';
import Footer from '../Footer/Footer';
import './PanouAutentificare.css';

function PanouAutentificare(){

    const [emailNou, setEmailNou] = useState("");
    const [parolaNoua, setParolaNoua] = useState("");
    const [telefonNou,setTelefonNou] = useState("");
    const [numeNou,setNumeNou] = useState("");
    const [prenumeNou,setPrenumeNou] = useState("");

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");


    let navigate=useNavigate();

    const register = () => {
        if(numeNou===''||prenumeNou===''||telefonNou===''||emailNou===''||parolaNoua==='')
            alert("Completati toate campurile.");
        else{
            Axios.post("http://localhost:3001/register", {
            Nume:numeNou,
            Prenume:prenumeNou,
            Telefon:telefonNou,
            Email:emailNou,
            Parola:parolaNoua,
            }).then((response) => {
            alert(response.data);
            if(response.data==='Cont client inserat cu succes.')
                window.location.reload();
            });
        }
    };
  
    const login = async (e) => {
        if(username===''||password==='')
            alert("Completati toate campurile.");
        else {
            e.preventDefault();
            try {
                const res = await Axios.post("http://localhost:3001/login", { username, password });
                window.localStorage.setItem("token",res.data.accessToken);
                window.localStorage.setItem("role",res.data.tipUtilizator);
                window.localStorage.setItem("id",res.data.id);
                if(res.data.tipUtilizator==='1')
                    navigate("/admin");
                if(res.data.tipUtilizator==='2')
                    navigate(`/panouDedicatAngajat/${res.data.id}`);
                if(res.data.tipUtilizator==='3')
                    navigate(`/panouDedicatClient/${res.data.id}`);
                if(res.data.message==='Datele de conectare nu sunt corecte. Reincercati!'||res.data.message==='Email-ul nu este corect. Reincercati!')
                    alert(res.data.message);
            } catch (err) {
              console.log(err);
            } 
        }   
    };

    /*
    Axios.get("http://localhost:3001/login").then((response) => {
          if (response.data.loggedIn == true) {
            setLoginStatus(response.data.user[0].Email);
            setRole(response.data.user[0].TipUtilizator);
            redirect(response.data.user[0].Cod_utilizator);
          }
    });
*/
  
    useEffect(() => {
        /*
        const checkIfLogin=()=>{
            if(window.localStorage.getItem("token"))
                {if(window.localStorage.getItem("role")==='1')
                    navigate("/admin/categorii")
                if(window.localStorage.getItem("role")==='2')
                    navigate(`/panouDedicatAngajat/${window.localStorage.getItem("id")}`);
                }
        }
        checkIfLogin();
        */
      },[]);
  
    return (
        <div className="mainDivPanouAutentificare">
            <div className="navBarDiv">
                <NavBar/>
            </div>
        <div className="divPanouAutentificareInregistrare">
            <div className="registration">
                <h1 className="Registration">Inregistrare cont nou</h1>
                <div className="NumeReg">
                    <label className="Nume">Nume: </label>
                    <input className="inputNumeReg"
                        type="text"
                        onChange={(e) => {
                        setNumeNou(e.target.value);
                        }}
                    />
                </div>
                <div className="PrenumeReg">
                    <label className="Prenume">Prenume: </label>
                    <input className="inputPrenumeReg"
                        type="text"
                        onChange={(e) => {
                        setPrenumeNou(e.target.value);
                        }}
                    />
                </div>
                <div className="TelefonReg">
                    <label className="Telefon">Telefon: </label>
                    <input className="inputTelefonReg"
                        type="text"
                        onChange={(e) => {
                        setTelefonNou(e.target.value);
                        }}
                    />
                </div>
                <div className="EmailReg">
                    <label className="Email">Email: </label>
                    <input className="inputEmailReg"
                        type="text"
                        onChange={(e) => {
                        setEmailNou(e.target.value);
                        }}
                    />
                </div>
                
                <br></br>
                <div className="ParolaReg">
                    <label className="Parola">Parola: </label>        
                    <input className="inputParolaReg"
                        type="password"
                        onChange={(e) => {
                        setParolaNoua(e.target.value);
                        }}
                    />
                </div>
                <br></br>
                <div className="ButtonReg">
                    <button onClick={register} className="registerButton"> INREGISTRARE CONT </button>
                </div>

            </div>
            <div className="login">
                <div className="EmailAut">
                    <label className="Email">Email: </label>
                    <input className="inputEmailReg"
                        placeholder="email..."
                        type="text"
                        onChange={(e) => {
                        setUsername(e.target.value);
                        }}
                    />
                </div>
                <div className="ParolaAut">
                    <label className="Parola">Parola: </label>        
                    <input className="inputParolaReg"
                        placeholder="parola..."
                        type="password"
                        onChange={(e) => {
                        setPassword(e.target.value);
                        }}
                    />
                </div>
                <button onClick={login} className="autentificareButon"> AUTENTIFICARE </button>
            </div>
      </div>
      <Footer/>
    </div>    
    );
}
export default PanouAutentificare;