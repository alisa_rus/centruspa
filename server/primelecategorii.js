module.exports = function(app){
const express = require('express');
const app = express();
const mysql = require("mysql");

app.use(express.json());
app.use(express.urlencoded({extended:true}));

const db = mysql.createPool({
    host:"localhost",
    user:"root",
    password:"",
    database:"centruspa_db",
});


app.get("/categorii",(req,res)=>{
    const query = "SELECT * from tipuri_servicii";
    db.query(query, (err,result) => {
        if(err) res.send(err);
        else res.send(result);
    })
});

app.post("/adaugareCategorie",(req,res) => {
    const Denumire= req.body.Denumire;
    const Descriere= req.body.Descriere;
    const Imagine = req.body.Imagine;
    const query1 = "SELECT * from tipuri_servicii where Denumire = ?";
    const query = "INSERT into tipuri_servicii(Denumire, Descriere, Imagine) VALUES (?,?,?)";
    db.query(query1,[Denumire], (err,result) => {
           
            if(result.length===0) {
                db.query(query,[Denumire, Descriere, Imagine],(err,result) => {
                                if(err) 
                                    res.send(err);
                                res.send(result);
                                });
                }
            else {
                res.send('no');
            }
    });
    
});
}
