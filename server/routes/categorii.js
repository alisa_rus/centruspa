const bodyParser = require("body-parser");
const express = require('express');
const fileUpload = require('express-fileupload');
const mysql = require("mysql");
const cors=require('cors');
const app = express();
const router = express.Router();

router.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
router.use(fileUpload());

const db = mysql.createPool({
    host:"localhost",
    user:"root",
    password:"",
    database:"centruspa",
});


router.get("/",(req,res)=>{
    const query = "SELECT * from categorii_servicii";
    db.query(query, (err,result) => {
        if(err) res.send(err);
        else res.send(result);
    })
});

router.get("/:id",(req,res)=>{
    const Cod_categorie=req.params.id;
    console.log(Cod_categorie);
    const query="SELECT * from categorii_servicii WHERE Cod_categorie=(?)";
    db.query(query,[Cod_categorie],(err,result)=>{
        if(err) req.send(err);
        else res.send(result);
    })
});

router.get("/getByName/:id",(req,res)=>{
    const Denumire=req.params.id;
    const query="SELECT * from categorii_servicii WHERE Denumire=(?)";
    db.query(query,[Denumire],(err,result)=>{
        if(err) req.send(err);
        else res.send(result);
    })
});

router.post("/adaugareCategorie",(req,res)=>{

    const file = req.files.file;
    const filename = file.name;
    const Denumire= req.body.Denumire;
    const Descriere= req.body.Descriere;
    uploadPath = 'D:/disk D/AN 4/SEM 1/LICENTA 2021/Aplicatie_CentruSPA/client/public/images/categorii/' + filename;
    const query1 = "SELECT * from categorii_servicii where Denumire = ?";
    const query2 = "INSERT into categorii_servicii(Denumire, Descriere, Imagine) VALUES (?,?,?)";
    db.query(query1,[Denumire], (err,result) => {

                if(result.length===0) {
                    db.query(query2,[Denumire, Descriere, filename],(err,result) => {
                                    if(err) 
                                        res.send("A aparut o eroare. Reincercati!");
                                    else
                                    {
                                            // Use mv() to place file on the server
                                            file.mv(uploadPath, function (err) {
                                                if(err) console.log(err);
                                                else {console.log('imagine adaugata')}
                                    })
                                    } 
                                    res.send("Categorie inserata cu succes.");
                                    });
                    }
                else {
                    res.send('Denumirea adaugata exista deja in baza de date. Va rugam alegeti alta denumire.');
                }
            });
    
});

router.put("/modificareCategorie",function(req,res){

    /*poate fi modificata si denumirea unei categorii, doar cu conditia ca noua denumire sa nu existe deja in db*/

    const filename = req.body.fileName;
    const Denumire= req.body.Denumire;
    const Descriere= req.body.Descriere;
    uploadPath = 'D:/disk D/AN 4/SEM 1/LICENTA 2021/Aplicatie_CentruSPA/client/public/images/categorii/' + filename;
    const Cod_categorie=req.body.Cod_categorie;
    const query="UPDATE  categorii_servicii SET Denumire=?, Descriere=?, Imagine=? WHERE Cod_categorie=?";
    const query2="SELECT * from categorii_servicii WHERE Denumire=? AND Cod_Categorie!=?";
    db.query(query2,[Denumire,Cod_categorie],(err,result)=>{
        if(result.length!==0)
            res.send("Denumirea exista deja. Alegeti alta denumire");
            else{
                db.query(query,[Denumire, Descriere,filename,Cod_categorie],(err,result)=>{
                    if(err) res.send("A aparut o eroare. Reincercati!.");
                    else {
                        // Use mv() to place file on the server
                        if(req.files!=null){
                        const file = req.files.file;
                        file.mv(uploadPath, function (err) {
                            if(err) console.log(err);
                        })
                        }
                        res.send("Categorie modificata cu succes.");
                        }
                });   
            }
    })
   
});

/*DE VERIFICAT*/
router.delete("/stergereCategorie/:id",function(req,res){

    const Cod_categorie=req.params.id;
    const query1="SELECT * from servicii s INNER JOIN programari p ON p.Cod_serviciu = s.Cod_serviciu WHERE s.Cod_categorie = ? AND p.Status != 'FINALIZATA'";
    const query2="DELETE programari from programari LEFT JOIN servicii on programari.Cod_serviciu = servicii.Cod_serviciu WHERE servicii.Cod_categorie = ?";
    const query5="DELETE servicii from servicii LEFT JOIN categorii_servicii on categorii_servicii.Cod_categorie = servicii.Cod_categorie WHERE categorii_servicii.Cod_categorie = ?";
    const query6="DELETE categorii_servicii from categorii_servicii WHERE categorii_servicii.Cod_categorie = ?";
    const query3="DELETE angajati_servicii from angajati_servicii LEFT JOIN servicii on angajati_servicii.ID_serviciu=servicii.Cod_serviciu WHERE servicii.Cod_Categorie=?";
    const query4="DELETE recenzii from recenzii LEFT JOIN servicii on recenzii.Cod_serviciu=servicii.Cod_serviciu WHERE servicii.Cod_categorie=?";
   
    db.query(query1, [Cod_categorie], (err, results) => {
        if(err)
        {
            res.send(err);
        } 
        else
        {
            if(results.length === 0)
            {
                db.query(query2, [Cod_categorie], (err, results) => {
                    if(err)
                    {
                        res.send("A aparut o eroare. Reincercati!.");
                    } 
                    else{
                        db.query(query3, [Cod_categorie], (err, results) => {
                        if(err)
                        {
                            res.send("A aparut o eroare. Reincercati!");
                        }
                        else{
                            db.query(query4, [Cod_categorie], (err, results) => {
                            if(err)
                            {
                                res.send("A aparut o eroare. Reincercati!");
                            } 
                            else{
                                db.query(query5, [Cod_categorie], (err, results) => {
                                if(err)
                                {
                                    res.send("A aparut o eroare. Reincercati!");
                                }  
                                else{
                                    db.query(query6, [Cod_categorie], (err, results) => {
                                    if(err)
                                    {
                                        res.send("A aparut o eroare. Reincercati!");
                                    } 
                                    else{
                                        res.send("Categorie stearsa cu succes.");
                                     }
                                });
                                }
                            });
                            }
                        });
                        }
                    });
                    }
                });        
            }
            else
            {
                res.send('Exista programari nefinalizate, categoria nu poate fi stearsa');
            }
        }
    });
});

module.exports = router;
