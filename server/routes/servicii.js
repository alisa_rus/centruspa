const bodyParser = require("body-parser");
const express = require('express');
const fileUpload = require('express-fileupload');
const mysql = require("mysql");
const cors=require('cors');
const app = express();
const router = express.Router();

router.use(cors());

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
router.use(fileUpload());

const db=mysql.createPool({
    host:"localhost",
    user:"root",
    password:"",
    database:"centruspa",
});

router.get("/",(req,res)=>{
    const query="SELECT * from servicii s";
    db.query(query,(err,result)=>{
        if(err) res.send(result);
        else res.send(result);
    })
});

router.get("/:id",(req,res)=>{
    const id=req.params.id;
    const query="SELECT * from servicii s WHERE s.Cod_serviciu = ?";
    db.query(query,[id],(err,result)=>{
        if(err) res.send(err);
        else res.send(result);
    });
});

router.get("/categoriaServiciului/:id",(req,res)=>{
    const id=req.params.id;
    const query="SELECT * from servicii s INNER JOIN categorii_servicii p ON p.Cod_categorie = s.Cod_categorie WHERE s.Cod_serviciu = ?";
    db.query(query,[id],(err,result)=>{
        if(err) res.send(err);
        else res.send(result);
    });
});

router.get("/dupaCategorie/:id",(req,res)=>{
    const id=req.params.id;
    const query="SELECT * FROM servicii where Cod_categorie=(?)";
    db.query(query,[id],(err,result)=>{
        if(err) res.send(result);
        else res.send(result);
    });
});

router.post("/adaugareServiciu",(req,res)=>{

    const query="INSERT into servicii(Cod_categorie, Denumire, Descriere, Beneficii, Durata, Pret, Imagine) VALUES (?,?,?,?,?,?,?)";
    const query1="SELECT * from servicii WHERE Denumire=(?)";
    //const query2="SELECT categorii_servicii.Cod_categorie from categorii_servicii WHERE Denumire=?";
    const Cod_categorie=req.body.Cod_categorie;
    const Denumire=req.body.Denumire;
    const Descriere=req.body.Descriere;
    const Beneficii=req.body.Beneficii;
    const Durata=req.body.Durata;
    const Pret=req.body.Pret;
    const file = req.files.file;
    const filename = file.name;
    let uploadPath='D:/disk D/AN 4/SEM 1/LICENTA 2021/Aplicatie_CentruSPA/client/public/images/servicii/' + filename;
    
    db.query(query1,[Denumire],(err,result)=>{
        if(result.length===0){
            if(isNaN(Pret)||isNaN(Durata)) res.send("Pretul si durata trebuie sa fie numere. Reincercati");
            else {
                db.query(query,[Cod_categorie,Denumire,Descriere,Beneficii,Durata,Pret,filename],(err,result)=>{
                    if(err) res.send("A aparut o eroare. Reincercati!");
                    else {
                        
                            // Use mv() to place file on the server
                            file.mv(uploadPath, function (err) {
                                if(err) console.log(err);
                                else {console.log('imagine adaugata')}
                            })
                  
                            res.send('Serviciu inserat cu succes.');
                    }
                    
                });
            }
        }
        else {
            res.send('Denumirea adaugata exista deja in baza de date. Va rugam alegeti alta denumire.');
        }
    });
});

router.put("/modificareServiciu",function(req,res){
    
    const filename = req.body.fileName;
    uploadPath = 'D:/disk D/AN 4/SEM 1/LICENTA 2021/Aplicatie_CentruSPA/client/public/images/servicii/' + filename;
    const Cod_serviciu=req.body.Cod_serviciu;
    const Denumire=req.body.Denumire;
    const Descriere=req.body.Descriere;
    const Beneficii=req.body.Beneficii;
    const Pret=req.body.Pret;
    const query="UPDATE servicii SET Denumire=?,Descriere=?, Beneficii=?, Pret=?, Imagine=? WHERE Cod_serviciu=?";
    const query1="SELECT * from servicii WHERE Denumire=(?) AND Cod_serviciu!=?";
    db.query(query1,[Denumire,Cod_serviciu],(err,result)=>{
        if(err) res.send("A aparut o eroare. Reincercati!");
        else if(result.length===0){
            if(isNaN(Pret)) res.send("Pretul trebuie sa fie un numar. Reincercati");
            else {
                db.query(query,[Denumire,Descriere,Beneficii, Pret,filename,Cod_serviciu],(err, result)=>{
                    if(err) res.send("A aparut o eroare. Reincercati!");
                    else 
                    {
                        // Use mv() to place file on the server
                        if(req.files!=null){
                            const file = req.files.file;
                            file.mv(uploadPath, function (err) {
                                if(err) console.log(err);
                            })
                            }
                        res.send('Serviciu modificat cu succes.');
                    }
                    
                });
            }
        } 
        else res.send('Denumirea adaugata exista deja in baza de date. Va rugam alegeti alta denumire.');
    })
   
});

router.delete("/stergereServiciu/:id", function(req,res){

    const Cod_Serviciu=req.params.id;
    const deleteAngajatiServicii="DELETE FROM  angajati_servicii Where ID_serviciu=?";
    const deleteRecenzii="DELETE FROM recenzii Where Cod_serviciu=?";
    const deleteProgramari="DELETE FROM programari WHERE Cod_serviciu=?";
    const deleteServicii="DELETE FROM servicii WHERE Cod_serviciu=?";
    const query1="SELECT * from servicii s INNER JOIN programari p ON p.Cod_serviciu = s.Cod_serviciu WHERE s.Cod_serviciu = ? AND p.Status != 'FINALIZATA'";
    
    db.query(query1,[Cod_Serviciu],(err,results)=>{

        if(err) res.send("A aparut o eroare. Reincercati!");

        else{

            if(results.length===0){

                db.query(deleteAngajatiServicii,[Cod_Serviciu],(err,results)=>{
                    if(err) res.send("A aparut o eroare. Reincercati!");
                    else{
                        db.query(deleteRecenzii,[Cod_Serviciu],(err,results)=>{
                        if(err) res.send("A aparut o eroare. Reincercati!");
                        else{  
                            db.query(deleteProgramari,[Cod_Serviciu],(err,results)=>{
                            if(err) res.send("A aparut o eroare. Reincercati!");
                            else{
                                db.query(deleteServicii,[Cod_Serviciu],(err,results)=>{
                                if(err) res.send("A aparut o eroare. Reincercati!");
                                else res.send("Serviciu sters cu succes.");
                                });
                            }
                            });                          
                        }
                        });                     
                    }
                });                 
            }
            else{
                res.send("Exista programari nefinalizate, serviciul nu poate fi sters.");
            }
        }
    });     
});

module.exports=router;