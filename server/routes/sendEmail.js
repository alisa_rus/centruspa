const express = require('express');
const bodyParser = require('body-parser');
const nodemailer = require('nodemailer');
const router=express.Router();
const app = express();
const cors=require('cors');
require('dotenv').config();
router.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/send', (req, res) => {

    let textEmail=req.body.textEmail;
    let destinatar=req.body.destinatar;

    let transporter = nodemailer.createTransport({
      service: 'Gmail',
      auth: {
          user: process.env.EMAIL,    // TODO: your gmail account
          pass: process.env.PASSWORD  // TODO: your gmail password
      }
  });
  
    // Step 2
    let mailOptions = {
        from: 'centruspatest@gmail.com', // TODO: email sender
        to: destinatar,                  // TODO: email receiver
        subject: 'Notificare Centru SPA',
        text: textEmail
    };
    
    // Step 3
    transporter.sendMail(mailOptions, (err, data) => {
        if (err) {
            res.send("A intervenit o eroare. Reincercati!")
        }
        else res.send("Email trimis.");
    });
  });

  module.exports=app;