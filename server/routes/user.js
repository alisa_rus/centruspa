const bodyParser = require("body-parser");
const express = require('express');
const fileUpload = require('express-fileupload');
const mysql = require("mysql");
const cors=require('cors');
const app = express();
const router = express.Router();
const bcrypt = require('bcrypt');
router.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
router.use(fileUpload());

app.use(express.json());
app.use(cors());

const db=mysql.createPool({
    host:"localhost",
    user:"root",
    password:"",
    database:"centruspa",
    dateStrings: 'date'
});

router.get("/echipa",(req,res)=>{
    const query = "SELECT * from utilizator u INNER JOIN info_angajati i Where i.Cod_angajat=u.Cod_utilizator AND u.TipUtilizator=(?)";
    db.query(query,[2], (err,result) => {
        if(err) res.send(err);
        else res.send(result);
    })
});

router.get("/angajatById/:id",(req,res)=>{
    const codAngajat=req.params.id;
    const query="SELECT * from utilizator u INNER JOIN info_angajati i WHERE u.Cod_utilizator=i.Cod_angajat AND u.Cod_utilizator=(?) ";
    db.query(query,[codAngajat],(err,result)=>{
        if(err) res.send(err);
        else res.send(result);
    })
});

router.get("/angajatByServiceId/:id",(req,res)=>{
    const codServiciu=req.params.id;
    const query="SELECT * from utilizator u INNER join angajati_servicii a WHERE u.Cod_utilizator=a.Cod_angajat AND a.ID_serviciu=(?)";
    db.query(query,[codServiciu],(err,result)=>{
        if(err) res.send(err);
        else res.send(result);
    })
})
router.get("/echipa/serviciiAngajat/:id",(req,res)=>{
    const codAngajat=req.params.id;
    const query="SELECT * from servicii s INNER JOIN angajati_servicii a WHERE s.Cod_serviciu=a.ID_serviciu AND a.Cod_angajat=(?)";
    db.query(query,[codAngajat],(err,result)=>{
        if(err) res.send(err);
        else res.send(result);
    })
});
router.get("/echipa/recenziiAngajat/:id",(req,res)=>{
    const codAngajat=req.params.id;
    const query="SELECT * from  recenzii a WHERE a.Cod_angajat=(?) ";
    db.query(query,[codAngajat],(err,result)=>{
        if(err) res.send(err);
        else res.send(result);
    })
});
router.get("/echipa/programariAngajat/:id",(req,res)=>{
    const codAngajat=req.params.id;
    const query="SELECT * from  programari a  INNER JOIN utilizator u INNER JOIN servicii s WHERE a.Cod_angajat=(?) and a.Status!='FINALIZATA' and u.Cod_utilizator=a.Cod_angajat and s.Cod_serviciu=a.Cod_serviciu ORDER BY a.Zi, a.OraStart ASC";
    db.query(query,[codAngajat],(err,result)=>{
        if(err) res.send(err);
        else res.send(result);
    })
});
router.post("/adaugareAngajat",async (req,res)=>{

    const servicii=req.body.Servicii;  //PRESUPUNEM CA SERVICIILE SUNT TRIMISE DIN FRONTEND CA UN VECTOR DE DENUMIRI
    const nume=req.body.Nume;
    const prenume=req.body.Prenume;
    const telefon=req.body.Telefon;
    const email=req.body.Email;
    const parola=await bcrypt.hash(req.body.Parola,10);
    const tipUtilizator=2; //2 inseamna angajat, 1 admin, 3 client
    const imagine=req.body.Imagine;
    const descriere=req.body.Descriere;
    const queryAddUtilizator="INSERT into utilizator(Nume,Prenume,Telefon,Email,Parola,TipUtilizator) VALUES (?,?,?,?,?,?)";
    const queryVerifyEmail="SELECT * from utilizator where Email=(?)";
    const queryGetCodUtilizator="SELECT Cod_utilizator from Utilizator Where Email=(?)";
    const queryAddInfoAngajati="INSERT into info_angajati(Cod_angajat,Imagine,Descriere) VALUES(?,?,?)";
    const queryAddAngajatiServicii="INSERT into angajati_servicii(Cod_angajat,ID_serviciu) VALUES(?,?)";
    const queryGetIdServiciu="SELECT Cod_serviciu from servicii WHERE Denumire=(?)";

    db.query(queryVerifyEmail,[email],(err,result)=>{
        if(result.length!==0) res.send('Emailul nu este disponibil. Alegeti alt email.');
        else{
            db.query(queryAddUtilizator,[nume,prenume,telefon,email,parola,tipUtilizator],(err,result)=>{
                if(err) res.send(err);
                else{
                    db.query(queryGetCodUtilizator,[email],(err,result)=>{
                        if(err) res.send(err);
                        else{
                            const a=JSON.stringify(result);
                            const b=JSON.parse(a);
                            const codAngajat=b[0].Cod_utilizator;
                            db.query(queryAddInfoAngajati,[codAngajat,imagine,descriere],(err,result)=>{
                            if(err) res.send(err);
                            else
                               {
                                 for (var i = 0; i < servicii.length; i++){
                                    db.query(queryGetIdServiciu,[servicii[i]],(err,result)=>{
                                        if(err) res.send(err);
                                        else{
                                            const a=JSON.stringify(result);
                                            const b=JSON.parse(a);
                                            const codServiciu=b[0].Cod_serviciu;
                                            db.query(queryAddAngajatiServicii,[codAngajat,codServiciu],(err,result)=>{
                                                if(err) res.send(err);    
                                            });
                                        }
                                    });
                                }                           
                               }      
                            });
                        }
                        // Use mv() to place file on the server
                        if(req.files!=null){
                        const file = req.files.file;
                        file.mv(uploadPath, function (err) {
                        if(err) console.log(err);
                        })
                        }
                        res.send("Angajat inserat cu succes.");
                    });
                }
            });      
        }     
    }); 
});

router.put("/angajat/modificareAngajat", async function(req,res){

    const FlagParola=req.body.Flag_parola;
    console.log(FlagParola);
    const Cod_utilizator=req.body.Cod_utilizator;
    const nume=req.body.Nume;
    const prenume=req.body.Prenume;
    const telefon=req.body.Telefon;
    const email=req.body.Email;
    const parola=await bcrypt.hash(req.body.Parola,10);
    const descriere=req.body.Descriere;
    const fileName=req.body.Imagine;
    let uploadPath = 'D:/disk D/AN 4/SEM 1/LICENTA 2021/Aplicatie_CentruSPA/client/public/images/echipa/' + fileName;
    //cu parola
    const query="UPDATE utilizator SET Nume=(?), Prenume=(?), Telefon=(?), Email=(?), Parola=(?) WHERE Cod_utilizator=(?)";
    //fara parola
    const query4="UPDATE utilizator SET Nume=(?), Prenume=(?), Telefon=(?), Email=(?) WHERE Cod_utilizator=(?)";
    const query2="UPDATE info_angajati SET Imagine=(?), Descriere=(?) WHERE Cod_angajat=(?)";
    /*email unic*/
    const query3="SELECT * from utilizator WHERE Email=(?) AND Cod_utilizator!=(?)";
    db.query(query3,[email,Cod_utilizator],(err,result)=>{
        if(err) res.send('A aparut o eroare. Reincercati!');
        else if(result.length!==0)
            res.send('Emailul introdus nu este disponibil. Reincercati!');
            else{
                if(FlagParola===true){
                db.query(query,[nume,prenume,telefon,email,parola,Cod_utilizator],(err,result)=>{
                    if(err) res.send('A aparut o eroare. Reincercati!');
                    else{
                        db.query(query2,[fileName,descriere,Cod_utilizator],(err,result)=>{
                            if(err) res.send(err);
                                else 
                                    {
                                        // Use mv() to place file on the server
                                        if(req.files!=null){
                                            const file = req.files.file;
                                            file.mv(uploadPath, function (err) {
                                                if(err) console.log(err);
                                            })
                                            }
                                        res.send("Cont angajat modificat cu succes.");
                                    }
                        })
                    }
                })}
                else{
                    db.query(query4,[nume,prenume,telefon,email,Cod_utilizator],(err,result)=>{
                        if(err) res.send('A aparut o eroare. Reincercati!');
                        else{
                            db.query(query2,[fileName,descriere,Cod_utilizator],(err,result)=>{
                                if(err) res.send(err);
                                    else 
                                        {
                                            // Use mv() to place file on the server
                                            if(req.files!=null){
                                                const file = req.files.file;
                                                file.mv(uploadPath, function (err) {
                                                    if(err) console.log(err);
                                                })
                                                }
                                            res.send("Cont angajat modificat cu succes.");
                                        }
                            })
                        }
                    })
                }
            }
    })
});

router.delete("/angajat/stergereAngajat/:id",function(req,res){

    const Cod_angajat=req.params.id;
    const query1="SELECT * from utilizator u INNER JOIN programari p ON p.Cod_angajat = u.Cod_utilizator AND p.Status != 'FINALIZATA' AND u.Cod_utilizator=(?)";
    const query2="DELETE programari from programari LEFT JOIN utilizator on programari.Cod_angajat = utilizator.Cod_utilizator WHERE utilizator.Cod_utilizator = ?";
    const query3="DELETE angajati_servicii from angajati_servicii LEFT JOIN utilizator on angajati_servicii.ID_serviciu=utilizator.Cod_utilizator WHERE angajati_servicii.Cod_angajat=?";
    const query4="DELETE recenzii from recenzii LEFT JOIN  utilizator on recenzii.Cod_angajat=utilizator.Cod_utilizator WHERE recenzii.Cod_angajat=?";
    const query5="DELETE info_angajati from info_angajati  WHERE info_angajati.Cod_angajat=(?)";
    const query6="DELETE utilizator from utilizator WHERE utilizator.Cod_utilizator=(?)"
    db.query(query1, [Cod_angajat], (err, results) => {
        if(err)
        {
            res.send(err);
            console.log(err);
        } 
        else
        {
            if(results.length === 0)
            {
                db.query(query2, [Cod_angajat], (err, results) => {
                    if(err)
                    {
                        res.send("A aparut o eroare. Reincercati!.");
                    } 
                    else{
                        db.query(query3, [Cod_angajat], (err, results) => {
                        if(err)
                        {
                            res.send("A aparut o eroare. Reincercati!.");
                        }
                        else{
                            db.query(query4, [Cod_angajat], (err, results) => {
                            if(err)
                            {
                                res.send("A aparut o eroare. Reincercati!.");
                            } 
                            else{
                                db.query(query5, [Cod_angajat], (err, results) => {
                                if(err)
                                {
                                    res.send("A aparut o eroare. Reincercati!.");
                                }  
                                else{
                                        db.query(query6, [Cod_angajat], (err, results) => {
                                        if(err)
                                        {
                                            res.send("A aparut o eroare. Reincercati!.");
                                        } 
                                        else
                                            res.send("Cont angajat sters cu succes."); 
                                     });
                                    }
                                });
                                }
                            });
                            }
                        });
                        }
                    });
                    }     
            else
            {
                res.send('Exista programari nefinalizate, angajatul nu poate fi sters.');
            }
        }
    });
});
router.get("/clienti",(req,res)=>{
    const query = "SELECT * from utilizator Where TipUtilizator=(?)";
    db.query(query,[3], (err,result) => {
        if(err) res.send(err);
        else res.send(result);
    })
});

router.get("/clientById/:id",(req,res)=>{
    const codClient=req.params.id;
    const query="SELECT * from utilizator WHERE Cod_utilizator=(?)";
    db.query(query,[codClient],(err,result)=>{
        if(err) res.send(err);
        else res.send(result);
    })
});

router.put("/clienti/modificareClient",async function(req,res){

    const nume=req.body.Nume;
    const prenume=req.body.Prenume;
    const telefon=req.body.Telefon;
    const email=req.body.Email;
    const parola=await bcrypt.hash(req.body.Parola,10);
    const Cod_utilizator=req.body.Cod_utilizator;
    const FlagParola=req.body.Flag_parola;

    const query="SELECT * from utilizator WHERE Email=(?) AND Cod_utilizator!=(?)";
    //cu parola
    const query2="UPDATE utilizator SET Nume=(?),Prenume=(?),Telefon=(?),Email=(?),Parola=(?) WHERE Cod_utilizator=(?)";
    //fara parola
    const query3="UPDATE utilizator SET Nume=(?),Prenume=(?),Telefon=(?),Email=(?) WHERE Cod_utilizator=(?)";
    db.query(query,[email,Cod_utilizator],(err,result)=>{
        if(err) res.send(err)
        else if(result.length!==0){
            res.send('Emailul introdus nu este disponibil. Reincercati!');
        }
        else{
            if(FlagParola===true){
            db.query(query2,[nume,prenume,telefon,email,parola,Cod_utilizator],(err,result)=>{
                if(err) res.send('A aparut o eroare. Reincercati!');
                else res.send('Cont client modificat cu succes.');
            });}
            else {
            db.query(query3,[nume,prenume,telefon,email,Cod_utilizator],(err,result)=>{
                if(err) res.send('A aparut o eroare. Reincercati!');
                else res.send('Cont client modificat cu succes.');
            });
            }
        }
    })

});
router.post("/clienti/adaugareClient",async (req,res)=>{

    const nume=req.body.Nume;
    const prenume=req.body.Prenume;
    const telefon=req.body.Telefon;
    const email=req.body.Email;
    const parola=await bcrypt.hash(req.body.Parola,10);
    const tipUtilizator=3; //2 inseamna angajat, 1 admin, 3 client
    const queryAddUtilizator="INSERT into utilizator(Nume,Prenume,Telefon,Email,Parola,TipUtilizator) VALUES (?,?,?,?,?,?)";
    const queryVerifyEmail="SELECT * from utilizator where Email=(?)";

    db.query(queryVerifyEmail,[email],(err,result)=>{
        if(result.length!==0) res.send('Emailul nu este disponibil. Alegeti alt email.');
        else{
            db.query(queryAddUtilizator,[nume,prenume,telefon,email,parola,tipUtilizator],(err,result)=>{
                if(err) res.send(err);
                else res.send("Cont client inserat cu succes.");
                    });
                }
        });           
});

router.delete("/clienti/stergereClient/:id",(req,res)=>{
    const Cod_utilizator=req.params.id;
    const query="DELETE FROM programari WHERE Cod_client=(?)";
    const query2="DELETE FROM utilizator WHERE Cod_utilizator=(?)";

    db.query(query,[Cod_utilizator],(err,result)=>{
        if(err) 
            res.send(err);
        else{
            db.query(query2,[Cod_utilizator],(err,result)=>{
                if(err)
                    res.send(err);
                else 
                    res.send("Cont client sters cu succes.");
            })
        }
    })
});

router.get("/clienti/programariClienti/:id",(req,res)=>{
    const codClient=req.params.id;
    const query="SELECT * from  programari a  INNER JOIN utilizator u INNER JOIN servicii s WHERE a.Cod_client=(?) and a.STATUS !='FINALIZATA' and u.Cod_utilizator=a.Cod_client and s.Cod_serviciu=a.Cod_serviciu ORDER BY a.Zi, a.OraStart ASC";
    db.query(query,[codClient],(err,result)=>{
        if(err) res.send(err);
        else res.send(result);
    })
});

router.get("/clienti/programariFinalizateClienti/:id",(req,res)=>{
    const codClient=req.params.id;
    const query="SELECT * from  programari a  INNER JOIN utilizator u INNER JOIN servicii s WHERE a.Cod_client=(?) and a.STATUS ='FINALIZATA' and u.Cod_utilizator=a.Cod_client and s.Cod_serviciu=a.Cod_serviciu ORDER BY a.Zi, a.OraStart ASC";
    db.query(query,[codClient],(err,result)=>{
        if(err) res.send(err);
        else res.send(result);
    })
});
module.exports=router;


