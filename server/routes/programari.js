const express=require('express');
const mysql=require('mysql');
const cors=require('cors');

const programari=express();
const router=express.Router();

router.use(cors());
  
programari.use(express.json());
programari.use(express.urlencoded({ extended: false }));

const db=mysql.createPool({
    host:"localhost",
    user:"root",
    password:"",
    database:"centruspa",
});

router.get("/",(req,res)=>{
    //const query="SELECT * from programari p  LEFT OUTER JOIN utilizator u ON u.Cod_utilizator=p.Cod_client  LEFT OUTER JOIN utilizator i ON i.Cod_utilizator=p.Cod_angajat INNER JOIN servicii s ON s.Cod_serviciu=p.Cod_serviciu";
    const query="SELECT * from programari";
    db.query(query,(err,result)=>{
        if(err) res.send(err);
        else res.send(result);
    })
});

router.get("/:id",(req,res)=>{
    const query="SELECT * from programari WHERE Cod_programare=(?)";
    db.query(query,[req.params.id],(err,result)=>{
        if(err) res.send(err);
        else res.send(result);
    })
});
router.delete("/stergereProgramare/:id",(req,res)=>{
    let Cod_programare=req.params.id;
    const query="DELETE from programari where Cod_programare=(?)";
    db.query(query,[Cod_programare],(err,result)=>{
        if(err) res.send(err);
        else res.send("Programare stearsa cu succes.");
    })
})
router.put("/confirmaProgramare/:id",(req,res)=>{
    let Cod_programare=req.params.id;
    const query="UPDATE programari SET STATUS='CONFIRMATA' Where programari.Cod_programare=(?)";
    db.query(query,[Cod_programare],(err,result)=>{
        if(err) res.send(err);
        else res.send("Programare confirmata cu succes. ");
    })
})
router.put("/finalizeazaProgramare/:id",(req,res)=>{
    let Cod_programare=req.params.id;
    const query="UPDATE programari SET STATUS='FINALIZATA' Where programari.Cod_programare=(?)";
    db.query(query,[Cod_programare],(err,result)=>{
        if(err) res.send(err);
        else res.send("Programare finalizata cu succes. ");
    })
})
router.post("/verificareProgramare",(req,res)=>{

    let Cod_angajat=req.body.Cod_angajat;
    let Zi=req.body.Zi;
    const query="SELECT * from programari WHERE programari.Cod_angajat=(?)  and programari.Zi=(?)";
    db.query(query,[Cod_angajat,Zi],(err,result)=>{
        if(err) res.send(err);
        else res.send(result);
    })
})
router.post("/adaugaProgramare",(req,res)=>{

    let {Cod_client,Cod_angajat,Cod_serviciu,Status,Zi,OraStart,Pret}=req.body;
    const query="INSERT into programari(Cod_client,Cod_angajat,Cod_serviciu,Status,Zi,OraStart,Pret) VALUES (?,?,?,?,?,?,?) ";
    db.query(query,[Cod_client,Cod_angajat,Cod_serviciu,Status,Zi,OraStart,Pret],(err,result)=>{
        if(err) res.send(err);
        else res.send("Programare finalizata cu succes.");
    })

})
module.exports=router;