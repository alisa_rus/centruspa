const express=require('express');
const mysql=require('mysql');
const cors=require('cors');

const recenzii=express();
const router=express.Router();

router.use(cors());
recenzii.use(express.json());
recenzii.use(express.urlencoded({ extended: false }));

const db=mysql.createPool({
    host:"localhost",
    user:"root",
    password:"",
    database:"centruspa",
});

router.get("/",(req,res)=>{
    const query="SELECT * from recenzii r INNER JOIN utilizator i ON i.Cod_utilizator=r.Cod_angajat INNER JOIN servicii s ON s.Cod_serviciu=r.Cod_serviciu";
    db.query(query,(err,result)=>{
        if(err) res.send(err);
        else res.send(result);
    });
});
router.delete("/stergereRecenzie/:id",(req,res)=>{
    let Cod_recenzie=req.params.id;
    const query="DELETE from recenzii where Cod_recenzie=(?)";
    db.query(query,[Cod_recenzie],(err,result)=>{
        if(err) res.send(err);
        else res.send("Recenzie stearsa cu succes.");
    })
});
router.post("/adaugaRecenzie",(req,res)=>{

    let {Cod_angajat,Cod_serviciu,Mesaj,Nota}=req.body;
    const query="INSERT into Recenzii(Cod_angajat,Cod_serviciu,Nota,Mesaj) VALUES (?,?,?,?) ";
    db.query(query,[Cod_angajat,Cod_serviciu,Nota,Mesaj],(err,result)=>{
        if(err) res.send(err);
        else res.send("Recenzie adaugata cu succes.");
    })

})
module.exports=router;

