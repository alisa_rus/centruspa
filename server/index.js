const categorii = require('./routes/categorii');
const servicii= require('./routes/servicii');
const user= require('./routes/user');
const programari=require('./routes/programari');
const recenzii=require('./routes/recenzii');
const sendEmail=require('./routes/sendEmail');

const express = require("express");
const mysql = require("mysql");
const cors = require("cors");

const bodyParser = require("body-parser");

const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const app = express();

app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use("/categorii",categorii);
app.use("/servicii",servicii);
app.use("/user",user);
app.use("/programari",programari);
app.use("/recenzii",recenzii);
app.use("/sendEmail",sendEmail);

const db=mysql.createPool({
    host:"localhost",
    user:"root",
    password:"",
    database:"centruspa",
});

app.get('/logout', function(req, res){
 
  if (req.session.user) {
    res.send({ loggedOut: true, user: ''});
  } else {
    res.send({ loggedOut: true });
  }

});

app.post("/register", (req, res) => {

    const nume=req.body.Nume;
    const prenume=req.body.Prenume;
    const telefon=req.body.Telefon;
    const email=req.body.Email;
    const parola=req.body.Parola;
    const tipUtilizator=3; //2 inseamna angajat, 1 admin, 3 client
    const queryAddUtilizator="INSERT into utilizator(Nume,Prenume,Telefon,Email,Parola,TipUtilizator) VALUES (?,?,?,?,?,?)";
    const queryVerifyEmail="SELECT * from utilizator where Email=(?)";

    bcrypt.hash(parola, 10, (err, hash) => {
        if (err) {
        console.log(err);
        }
        db.query(queryVerifyEmail,[email],(err,result)=>{
            if(result.length!==0) res.send('Emailul nu este disponibil. Alegeti alt email.');
            else{
                db.query(queryAddUtilizator,[nume,prenume,telefon,email,hash,tipUtilizator],(err,result)=>{
                    if(err) res.send(err);
                    else res.send("Cont client inserat cu succes.");
                        });
                    }
        });    
    });
        
});

app.get("/login", (req, res) => {
  if (req.session.user) {
    console.log(req.session.user);
    res.send({ loggedIn: true, user: req.session.user });
  } else {
    res.send({ loggedIn: false });
  }
});

const generateAccessToken = (user) => {
  return jwt.sign({ id: user.id, isAdmin: user.isAdmin }, "mySecretKey", {
    expiresIn: "300s",
  });
};

let refreshTokens = [];
const generateRefreshToken = (user) => {
  return jwt.sign({ id: user.id, isAdmin: user.isAdmin }, "myRefreshSecretKey");
};

app.post("/login", (req, res) => {
  const username = req.body.username;
  const password = req.body.password;

  db.query(
    "SELECT * FROM utilizator WHERE Email = ?;",
    username,
    (err, result) => {
      if (err) {
        res.send({ err: err });
      }

      if (result.length > 0) {
        bcrypt.compare(password, result[0].Parola, (error, response) => {
          if (response) {
            const user=result;
            const accessToken = generateAccessToken(user);
            const refreshToken = generateRefreshToken(user);
            refreshTokens.push(refreshToken);
            res.json({
              email: user[0].Email,
              tipUtilizator:user[0].TipUtilizator,
              id:user[0].Cod_utilizator,
              mesaj:"Autentificare realizata cu succes!",
              accessToken,
              refreshToken,
          })} else {
            res.send({ message: "Datele de conectare nu sunt corecte. Reincercati!" });
          }
        });
      } else {
        res.send({ message: "Email-ul nu este corect. Reincercati!" });
      }
    }
  );
});

const verify = (req, res, next) => {
  const authHeader = req.headers.authorization;
  if (authHeader) {
    const token = authHeader.split(" ")[1];

    jwt.verify(token, "mySecretKey", (err, user) => {
      if (err) {
        return res.status(403).json("Token is not valid!");
      }

      req.user = user;
      next();
    });
  } else {
    res.status(401).json("You are not authenticated!");
  }
};

app.post("/logout", verify, (req, res) => {
  const refreshToken = req.body.token;
  refreshTokens = refreshTokens.filter((token) => token !== refreshToken);
  res.status(200).json("You logged out successfully.");
});

app.listen(3001, () => {
  console.log("running server");
});